package com.dw.automation.pages.base;

import static com.scholastic.torque.common.TestBaseProvider.getTestBase;

import com.dw.automation.pages.ILoginPage;
import com.dw.automation.pages.IProfilePage;
import com.dw.automation.pages.impl.desktop.LoginPageImplDesktop;
import com.dw.automation.pages.impl.desktop.ProfilePageImplDesktop;

public abstract class PageFactory {

	public static PageFactory getFactory() {
		String platform = getTestBase().getContext().getString("platform", "");
		if (platform.equalsIgnoreCase("desktop")) {
			return new BrowserPageFactory();
		}
		return null; 
	}

	public abstract ILoginPage getILoginPage();
	
	public abstract IProfilePage getIProfilePage();
	

	

	

	public static class BrowserPageFactory extends PageFactory {

		@Override
		public ILoginPage getILoginPage() {
			return new LoginPageImplDesktop();
		}

		@Override
		public IProfilePage getIProfilePage() {
			return new ProfilePageImplDesktop();
		}
		
		




	}




}
