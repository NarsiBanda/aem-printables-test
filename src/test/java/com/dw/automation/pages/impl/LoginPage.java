package com.dw.automation.pages.impl;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import com.dw.automation.pages.ILoginPage;
import com.dw.automation.support.PauseUtil;
import com.dw.automation.support.SCHUtils;
import com.scholastic.torque.common.AssertUtils;
import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.TestPage;
import com.scholastic.torque.common.WaitUtils;

import junit.framework.Assert;

public class LoginPage extends BaseTestPage<TestPage> implements ILoginPage {

	@FindBy(locator = "dw.home.txt.username")
	private WebElement txtUsername;
	
	@FindBy(locator = "dw.home.txt.username")
	private List<WebElement> sizeTxtUsername;

	@FindBy(locator = "dw.home.txt.password")
	private WebElement txtPassword;

	@FindBy(locator = "dw.home.btn.signon")
	private WebElement btnSignOn;
	
	@FindBy(locator="dw.login.close.icon")
	private WebElement iconcloseloginpage;
	
	@FindBy(locator="dw.home.homeicon")
	private WebElement homeicon;
	
	@Override
	public WebElement getTxtUsername() {
		return txtUsername;
	}
	
	@Override
	public List<WebElement> sizeTxtUsername() {
		return sizeTxtUsername;
	}

	@Override
	public WebElement getTxtPassword() {
		return txtPassword;
	}

	@Override
	public WebElement getBtnSignOn() {
		return btnSignOn;
	}
	
	public WebElement gethomeicon(){
		return homeicon;
	}
	
	public WebElement geticoncloseloginpage(){
		return iconcloseloginpage;
	}
	
	
	@Override
	public void launchPage() {
		openPage();
	}
    
	@Override
	public void openPage() {
			PauseUtil.pause(6000);
			WebElement el = geticoncloseloginpage();
			PauseUtil.pause(5000);
			if (el.isDisplayed()) {
				el.click();
				WaitUtils.waitForEnabled(getTxtUsername());
			}
		}
	
	
	@Override
	public void doLogin(String username, String password, boolean isRemember) {

		PauseUtil.pause(2000);
		WaitUtils.waitForEnabled(getTxtUsername());
		getTxtUsername().click();
		getTxtUsername().sendKeys(username);
		WaitUtils.waitForEnabled(getTxtPassword());
		PauseUtil.pause(1000);
		getTxtPassword().click();
		getTxtPassword().sendKeys(password);
		PauseUtil.pause(1500);
		
		SCHUtils.clickUsingJavaScript(
	    SCHUtils.waitForElementToBeClickable(getBtnSignOn(), 30));
		PauseUtil.pause(4000);
		

	}
	
	
	@Override
	public void verify_homepage(){
		
	PauseUtil.waitForAjaxToComplete(2500);
	PauseUtil.pause(4500);
	
	AssertUtils.assertDisplayed(gethomeicon());
		
	}

}