package com.dw.automation.pages;

import java.util.List;

import org.openqa.selenium.WebElement;

public interface ILoginPage {

	void launchPage();

	void doLogin(String username, String password, boolean isRemember);
	
	WebElement getTxtUsername();

	WebElement getTxtPassword();

	WebElement getBtnSignOn();
	
	List<WebElement> sizeTxtUsername();

	void verify_homepage();

	
	
}
