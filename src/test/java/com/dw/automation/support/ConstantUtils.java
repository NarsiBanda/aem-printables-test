package com.dw.automation.support;

import com.scholastic.torque.common.TestBaseProvider;

import antlr.collections.List;

public class ConstantUtils {
	
	public static final String QASTUDENTINCENTIVEMAXNUMBERERRORMESSAGE="You have exceeded the maximum amount of students for one order. "
			+ "You cannot submit a request for more than 50 students. If you need additional student incentives, please contact Customer Service at 1-800-268-3860.";
	
	public static final String ENTERORDER = "Enter Classroom Order";
	public static final String TEACHERCATALOGUES = "Teacher Catalogues";
	public static final String SHOP_BOOK_AND_RESOURCES = "Shop Books & Resources";
	public static final String TEACHER_DESK = "Teacher's Desk";
	public static final String PROFILE = "Profile";
	public static final String SFO_CART_PRODUCTS = "testexecution.SFOCartProducts";
	public static final String YTO_CART_PRODUCTS = "testexecution.YTOCartProducts";
	public static final String PRODUCT_DETAIL_PAGE = "testexecution.PARENTCartProducts";
	public static final String PO_CART_PRODUCTS = "testexecution.POCartProducts";
	public static final String POSOO_CART_PRODUCTS = "testexecution.POSOOCartProducts";
	public static final String DEFAULT_STUDENT_NAME_TEXT =
			"e.g., John S., J.S., or alias";
	public static final String DEFAULT_ITEM_ID_TEXT = "e.g., 1H31";
	public static final String DEFAULT_STUDENT_NAME_TEXT_ON_COUPON =
			"e.g. John S, J.S., or alias";
	public static final String DEFAULT_COUPON_ITEM_ID_TEXT = "e.g.3P3RW";
	public static final String SFO_REMOVED_CART_PRODUCTS =
			"testexecution.SFOCartRemovedProducts";
	public static final String SFO_ORDER_SUMMARY_TOTAL = "sfo total";
	public static final String SFO_SUBTITLE = "Student Flyer Orders";
	public static final String YTO_SUBTITLE = "Teacher Order";
	public static final String PAYPAL = "PayPal";
	public static final String CREDITCARD = "Credit Card";
	public static final String ADDNEWCARD = "Add a New Card";
	public static final String CCVISCARD  = "Visa ************1111";
	public static final String USER_PROFILE = "userProfile";
	public static final String ORDERSUMMARY_TEXT = "Order Summary";
	public static final String PAYMENT_TEXT = "Payment";
	public static final String SHIPPING_TEXT = "Shipping";
	public static final String ORDERDETAIL_TEXT = "ORDER DETAIL";
	public static final String CONFIRMANDSUBMITORDER = "Submit Your Order";
	public static final String REWARDSANDCOUPONS = "Redemptions & Rewards";
	public static final String SHIPPINGANDPAYMENT = "Shipping & Payment";
	public static final String REVIEWCARTPAGE = "Review Your Cart";
	public static final String THANKYOUFORORDER_TEXT = "Thank You for Your Order!";
	public static final String ORDERRECEIPTREFERENCE_TEXT = "ORDER RECEIPT REFERENCE";
	public static final String CHECKPAYMENTMETHOD = "Cheques";
	public static final String CCANDCHECKPAYMENTMETHOD = "Combination of Credit Card and Cheques";
	public static final String SOO_CART_PRODUCTS = "testexecution.SOOCartProducts";
	public static final String SSO_SUBTITLE = "Student Online Orders";
	public static final String CHECKTEXT = "MAKE CHECKS PAYABLE TO:";
	public static final String CHECKTOADD1 = "Scholastic";
	public static final String CHECKTOADD2 = "P.O. Box 7503";
	public static final String CHECKTOADD3 = "Jefferson City, MO 65102";
	public static final String CHECKINSTRUCTION =
			"Please submit this order, then use postpaid envelope provided in any Reading Club catalog to mail your checks and payment stub.";
	public static final String INSUFFICIENTBONUSPOINTCHECKOUTERRORMESSAGE =
			"There are not enough Bonus Points in your bank to complete the order.";
	public static final String SOO_SUBTITLE = "Student Online Orders";
	public static final String CCAMOUNT = "1.0";
	public static final String YTO_ORDER_SUMMARY_TOTAL = "yto total";
	public static final String ITEM_FLYER_TEXT = "Bonus Catalog";
	public static final String SOO_REMOVED_CART_PRODUCTS =
			"testexecution.SOOCartRemovedProducts";
	public static final String SFO_SELECTED_ROSTER_NAME =
			"testexecution.SFO_selected_name";
	public static final String YTO_ITEMDETAILS_CART = "testexecution.YToItemDetailsCart";
	public static final String YTO_REMOVED_CART_PRODUCTS =
			"testexecution.YTOCartRemovedProducts";
	public static final CharSequence FREESHIPPING = "FREE SHIPPING";
	public static final CharSequence SECONDDAYBONUS =
			"2-Business Day with specific bonus point";
	public static final CharSequence SECONDDAYSHIPPING =
			"2-Business Day with specific amount";
	public static final String SETDUEDATE = "Set";
	public static final String SFO_TOTAL_PRICE = "SFO total price";
	public static final String SFO_TOTAL_QUANTITY = "SFO total quantity";
	public static final String SHIPPING = "Shipping";
	public static final String CHILDTEACHERINFO = "testexecution.ChildTeacherInfo";
	public static final String PAYMENT_METHOD_UPDATED = "Payment method updated";
	public static final String PAYPALPAYMENTMETHOD = "PayPal Account";
	public static final String CHECKSANDCREDITCARD =
			"Combination of Credit Card and Checks";
	public static final String TOTAL_AMOUNT_DUE = "Total ";
	public static final String SUBTOTAL = "Subtotal";
	public static final String PURCHASEORDERPAYMENTMETHOD = "Purchase Order";
	public static final String YTO_ITEMID = "";
	public static final String COUPONS_PRODUCTS = "testexecution.CouponsProducts";
	public static final String INVALIDEMAILIDMESSAGE =
			"Please enter a valid email address.";
	public static final String INVALIDCREDENTIALSMESSAGE1_PCOD = "Log-In Error";
	public static final String INVALIDCREDENTIALSMESSAGE2_PCOD =
			"We couldn't find an account with the information you provided. Please try again or click the \"Forgot your account information?\" link below to retrieve your account information.";
	public static final String VALIDATIONMSG_LASTNAME_ADDACHILD = "Please enter at least 2 characters";
	public static final String INVALIDPHNNUMMESSAGE =
			"Please enter a valid phone number in the following format: 111-222-3333";
	public static final String CHECK = "Checks";
	public static final String SFO_COUPONS_FLAG = "false";
	public static final String INVALID_COUPON_MSG =
			"Please check the coupon and try again.";
	public static final String ORDER_NOT_MEET_COUPON_REQUIREMENTS_MSG =
			"Your order does not meet coupon requirements. Please check the coupon for details.";
	
	public static final String ORDER_NOT_MEET_FREEPICKCOUPON_REQUIREMENTS_MSG =
			"Your order does not meet the \"ISNOQYO\" coupon requirements. Please check the coupon for details.";

	public static final String INVALID_CUSTOMER_NO_MSG =
			"ID number is invalid. Please check and try again.";
	public static final String INCOMPLETE_USER_INFO =
			"Click the coupon link below to finalize your coupons";

	public static final String TOSS_IN_TEXT = "You've earned a pack";
	public static final String FREE_PRAMOTION_TEXT_CONFIRM_AND_SUBMIT_PAGE = "You've earned a pack";
		
	public static final String COUPON_REMOVED_ITEMS = "testexecution.CouponRemovedItems";
	public static final String MAXIMUM_STUDENT_LIMIT_ERROR_MESSAGE =
			"Maximum number of students cannot exceed \"50\".";
	public static final String PASSWORD = "passw0rd";
	public static final String BEGINNING_BONUS_POINTS = "Beginning Bonus Points";
	public static final String POINTS_PER_DOLLAR_SPENT = "x Points Per Dollar";
	public static final String EXTRA_BONUS_POINTS_REWARDS = "Extra Points";
	public static final String BONUS_POINTS_COUPON_REDEEMED = "Coupons";
	public static final String TOTAL_BONUS_POINTS_EARNED = "Total Bonus Points Earned";
	public static final String BONUS_POINTS_USED = "Bonus Points Used";
	public static final String ENDING_BONUS_POINT_BALANCE = "Ending Bonus Point Balance";
	public static final String BONUSPOINTS_BANK_VALUE = "";
	public static final String DEFAULT_PAYMENT_METHOD_OPTION = "Select Payment Method";
	public static final String NO_REWARDS = "There are no rewards to review";
	public static final String REWARDS_EARN_TODAY =
			"Rewards Earned with Today's Order of";
	public static final CharSequence COMBINEDORDERFREESHIPPING =
			"combined free order shipping method";
	public static final String PO_CART_DONATED_PRODUCTS =
			"testexecution.POCartDonatedProducts";
	public static final String THREE_COUPONS_APPLIED_TEXT =
			"Please note: Only three coupons can be applied to an order.";
	public static final String ADDNEWCHILD = "Add a Child";
	public static final String EMPTY_COUPONS_SEARCH_MESSAGE = "No search results found";
	public static final String BOOKFORTRUSTTOOLTIPTEXT =
			"100% of your donation goes to Book Trust , a charity that lets kids in need choose and own books from Scholastic Reading Club for free.";
	public static final String SOO_DELETE_OPTION_HEADER =
			"Please tell us why you're deleting this order";
	public static final String SEARCH_BOX_TEXT =
			"Search by Title, Author, Item # or Keyword";
	public static final String MY_ACCOUNT = "My Account"; //
	public static final String MY_LIST = "My Lists";
	public static final String HELP = "Help";
	public static final String EDIT_ONLINE_NOTE_TO_PARENT_DIALOG_TITLE =
			"Edit Online Note to Parents";
	public static final String EDIT_ONLINE_NOTE_TO_PARENT_DIALOG_MAX_MSG_LIMIT =
			"max 150";

	public static final String SOO_HEADER_ITEM = "ITEM #";
	public static final String SOO_HEADER_TITLE = "TITLE";
	public static final String SOO_HEADER_FLYER = "FLYER";
	public static final String SOO_HEADER_PRICE = "PRICE";
	public static final String SOO_HEADER_QTY = "QTY";
	public static final String ORDER_STATUS = "Order Status";
	public static final String FOR_TEACHERS = "FOR TEACHERS";
	public static final String ACCOUNT_INFORMATION = "ACCOUNT INFORMATION";
	public static final String EMAIL_REQUIRED_ERROR_MESSAGE =
			"Please enter your email address.";
	public static final String PASSWORD_REQUIRED_ERROR_MESSAGE =
			"Please enter your password.";
	public static final String INVALID_EMAIL_ERROR_MESSAGE =
			"Please enter a valid email address.";
	public static final String INVALID_CREDENTIALS_ERROR_MESSAGE = "Log-In Error";
	public static final String RECOMMENDATIONS = "MOST RECOMMENDED by";

	public static final String PARENT_REMOVED_ITEMS = "testexecution.ParentRemovedItems";
	public static final String RECOMMANDATIONS_FOR_STUDENT =
			"Recommendations for Your Students";

	public static final String FREE_BOOKTRUST_ITEMID = "066063";
	public static final String YTO_INSTRUCTION_TEXT =
			"Enter items for your classroom the usual way, BY FLYER, or the new way, BY ITEM #, using dollars or Bonus Points. You'll be asked to enter coupons at checkout.";
	public static final String REGSECTION4TITLE = "Almost There!";
	public static final String INVALID_NAME_ERROR_MESSAGE = "Please enter valid name.";
	public static final String INVALID_PASSWORD_ERROR_MESSAGE =
			"Please make sure your password has at least 7 characters and contains both letters and numbers.";
	public static final String ALREADY_EXIST_EMAIL_ERROR_MESSAGE =
			"An account associated with this email already exists. Please sign in. Forgot your password?";
	public static final String OPTIONAMPRIMARYGRADEMSG =
			"Please select the primary grade you teach to ensure you receive the promotional items most appropriate for your students and help us customize your online experience (optional).";
	public static final String BOOK_TRUST_TEXT = "Make a Donation to Help Kids in Need";
	public static final String REGSEC4SUBHEADER =
			"Customize your Reading Club experience:";
	public static final String REGSEC4FIRSTYEARTEXT = "This is my first year teaching.";
	public static final String REGSEC4READINGCLUBTEXT =
			"I have ordered from Reading Club (or Book Clubs) before.";
	public static final String REGSEC4NEWTEACHER =
			"I am mentoring a new teacher this year.";
	public static final String DEFAULT_SELECT_TYPE = "Select Type";
	public static final String DEFAULT_MONTH_SELECT = "Month";
	public static final String DEFAULT_DAY_SELECT = "Day";
	public static final String TEACHER_DEFAULT_MESSAGE_ON_PARENT_HOMEPAGE =
			"Welcome to our classroom's Reading Club home page! You'll find great books at affordable prices that your child will love to read! Plus every order helps our class earn FREE books!";
	public static final String ADD_TO_WISHLIST_TITLE = "Add to My Lists";
	public static final String EDITPROFILE = "Edit Profile";
	public static final String CHANGEPASSWORDINSTRCTION =
			"For security, your password must have at least 7 characters and contain both letters and numbers, e.g., Scholastic123.";
	public static final String INVALIDPASSWORDERRORMSG =
			"Please make sure your password has at least 7 characters and contains both letters and numbers.";
	public static final String CHILD_DELETE_CONFIRMATION_MSG =
			"Are you sure you want to remove this child from your account?";
	public static final String PAYPAL_REMOVE_WARNING_MSG =
			"Are you sure you want to delete this";
	public static final String WISHLIST_PRODUCT_ITEMS = "";
	public static final String WISHLIST_REMOVED_PRODUCT_ITEMS = "";

	public static final String ADD_TO_MY_LIST = "Add to My Lists";
	public static final String CREDIT_CARD_LIMIT_ERROR_MESSAGE =
			"You can only store three credit cards";
	public static final String SELECTED_LIST_SIZE = "";
	public static final String RECOMMENDATIONS_FOR_STUDENTS =
			"Recommendations for Students";
	public static final String Checked_Field = "checked";
	public static final String Disable_Field = "disabled";
	public static final String YOUR_TEACHER_RECOMMENDATIONS = "";
	public static final String TEACHER_MY_LIST = "teacher_my_list";
	public static final String YTO_FLYER_NAME = "Kindergarteners";
	public static final String CONFIRMATION_INFO = "";
	public static final String BOUNCING_ALERT_TEXT =
			"One or more students in your classroom have submitted online orders to you";
	public static final String SHIPPING_CHARGES_BEFORE_PayPal = "0.0";
	public static final String NO_ORDER = "No Orders";
	public static final String ORDER_STATUS_SUBMITTED = "submitted";
	public static final String ORDER_STATUS_TEACHER_SUBMITTED = "teacher submitted";
	public static final String ORDER_STATUS_TRANSIT = "in transit";
	public static final String ORDER_STATUS_DELIVERED = "delivered";
	public static final String CLASS_WISHLIST_SECTION_HEADER = "Classroom Wish List";
	public static final String DONATION_SECTION_HEADER = "Donations";
	public static final String STUDENT_FLYER_ORDER = "Student Flyer Orders";
	public static final String STUDENT_ONLINE_ORDER = "Student Online Orders";
	public static final String YOUR_TEACHER_ORDER = "Your Teacher Order";
	public static final String PRE_ORDER = "PRE-ORDER";
	public static final String INVALID_STUDENTNAME_ERROR =
			"Please enter a valid Student Name.";
	public static final String OUT_OF_STOCK_MESSAGE =
			"This item is temporarily out of stock";
	public static final String DOWNLOADPDFURl =
			"https://int-api.scholastic-labs.com/qa1/bpb-clubs-service/1.0/HTMLtoPDFConvertor";
	public static final String MYLIST_SIZE_BEFORE_ADD_TO_CART = "";
	public static final String Coupon_Finalize_Error_Message =
			"Click the coupon link below to finalize your coupons";
	public static final String PARENT_MY_LIST = "parent_my_list";
	public static final String REWARDS_HEADER_LABEL = "to spend right now";
	public static final String TEACHER_WISHLIST_HEADER = "YOUR TEACHER WISH LIST ITEMS";
	public static final String TEACHER_DESK_NO_RECOMMENDATIONS_ITEM_HEADER =
			"Make Recommendations to Students";
	public static final String TEACHER_DESK_NO_ADDTOWISHLIST_ITEM_HEADER =
			"Start a Wish List for Your Classroom";
	public static final String TEACHER_DESK_RECOMMENDATIONS_ITEM_HEADER =
			"Make Recommendations";
	public static final String TEACHER_DESK_ADDTOWISHLIST_ITEM_HEADER =
			"Add to Wish List";
	public static final String TEACHER_DESK_SAVEFORLATER_ITEM_HEADER = "Save for Later";
	public static final String CART_VALUE_BEFORE_ITEM_ADD_AT_UNAUTHENTICATE_PAGE = "";
	public static final String CART_PAGE_WISHLIST_HEADER = "WishList Item";
	public static final String Wrong_Coupon_Error_Message =
			"The system does not recognize the coupon code you have entered. Please check the coupon and try again. If the coupon code is correct, please contact Customer Service at 1-800-SCHOLASTIC (1-800-724-6527).";


}
