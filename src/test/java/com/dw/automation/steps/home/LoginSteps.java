package com.dw.automation.steps.home;

import static com.dw.automation.pages.base.PageFactory.getFactory;

import com.dw.automation.pages.ILoginPage;
import com.dw.automation.support.PauseUtil;
import com.scholastic.torque.common.TestBaseProvider;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class LoginSteps {
	@Given("^teacher open rco-scholastic web site$")
	public void user_open_rco_scholastic_web_site() {
		ILoginPage homePage = getFactory().getILoginPage();
		homePage.launchPage();
		PauseUtil.waitForAjaxToComplete(4000);
		
	}
	
	@Given("^teacher have valid credentials$")
    public void teacher_have_valid_credentials() throws Throwable {
        System.out.println("teacher have valid credentials");
    }
	
	
	@Given("^teacher login into the application by providing \\[UserInformation\\]$")
    public void teacher_login_into_the_application_by_providing_userinformation() throws Throwable {
		ILoginPage homePage = getFactory().getILoginPage();
		System.out.println("User ID......................"
				+ TestBaseProvider.getTestBase().getTestData().getString("userid"));
		System.out.println("PAssword..................."
				+ TestBaseProvider.getTestBase().getTestData().getString("password"));
		homePage.doLogin(TestBaseProvider.getTestBase().getTestData().getString("userid"),
				         TestBaseProvider.getTestBase().getTestData().getString("password"),
				false);
    }
	
	
	@Then("^teacher should navigate to home page$")
    public void teacher_should_navigate_to_home_page() throws Throwable {
		ILoginPage homePage = getFactory().getILoginPage();
        homePage.verify_homepage();
    }

    	
	
	
}
