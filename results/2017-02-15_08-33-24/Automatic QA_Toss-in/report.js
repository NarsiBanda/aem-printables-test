$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("features/testfeature.feature");
formatter.feature({
  "line": 2,
  "name": "Auto Toss-in",
  "description": "This feature is used to check for the Automatic Toss-in features",
  "id": "auto-toss-in",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@Automatic_Q_A_Toss-in"
    },
    {
      "line": 1,
      "name": "@P1"
    },
    {
      "line": 1,
      "name": "@Regression"
    },
    {
      "line": 1,
      "name": "@Android"
    },
    {
      "line": 1,
      "name": "@IOS"
    },
    {
      "line": 1,
      "name": "@Web"
    }
  ]
});
formatter.scenarioOutline({
  "comments": [
    {
      "line": 37,
      "value": "########################## Teacher Checkout Test Case ################################"
    }
  ],
  "line": 39,
  "name": "Pop up seen when clicked on the Reading Level in PDP page:[\u003cUsingData\u003e]",
  "description": "",
  "id": "auto-toss-in;pop-up-seen-when-clicked-on-the-reading-level-in-pdp-page:[\u003cusingdata\u003e]",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 41,
  "name": "Parent login in with valid [username] and [password]",
  "keyword": "Given "
});
formatter.step({
  "line": 42,
  "name": "parent searches with valid item which is having reading level information",
  "keyword": "And "
});
formatter.step({
  "line": 43,
  "name": "PDP of the item displays",
  "keyword": "And "
});
formatter.step({
  "line": 44,
  "name": "parent click on Reading Level line",
  "keyword": "When "
});
formatter.step({
  "line": 45,
  "name": "pop up is displayed with reading level infomation",
  "keyword": "Then "
});
formatter.step({
  "line": 46,
  "name": "“About Reading Levels” link is present in the pop up",
  "keyword": "And "
});
formatter.examples({
  "line": 48,
  "name": "",
  "description": "",
  "id": "auto-toss-in;pop-up-seen-when-clicked-on-the-reading-level-in-pdp-page:[\u003cusingdata\u003e];",
  "rows": [
    {
      "cells": [
        "UsingData"
      ],
      "line": 49,
      "id": "auto-toss-in;pop-up-seen-when-clicked-on-the-reading-level-in-pdp-page:[\u003cusingdata\u003e];;1"
    },
    {
      "cells": [
        "InSprint13.1:PCOD:Parent_PDPPage_ReadingLevelWindowPopup"
      ],
      "line": 50,
      "id": "auto-toss-in;pop-up-seen-when-clicked-on-the-reading-level-in-pdp-page:[\u003cusingdata\u003e];;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 31432167724,
  "status": "passed"
});
formatter.background({
  "comments": [
    {
      "line": 5,
      "value": "#[UserInformation]- UserName,Password"
    },
    {
      "line": 6,
      "value": "#[ItemInformation]-StudentName,ItemId"
    },
    {
      "line": 7,
      "value": "#[OrderSummary]-Bonus Points Used ,Bonus Points Earned ,Student Online Orders (Paid) ,Student Flyer Orders,Your Teacher Order"
    },
    {
      "line": 8,
      "value": "#[OrderInformation]-ItemId , Quantity and other attributes related to item."
    },
    {
      "line": 9,
      "value": "#[Free_Shipping_msg]- \u0027shipping fee is $3.50\u0027"
    },
    {
      "line": 10,
      "value": "#[Shipping_Charge_Msg]- \u0027Shipping charge as $3.50\u0027"
    },
    {
      "line": 11,
      "value": "#[Due_date_message] - \u0027Due Day message\u0027"
    },
    {
      "line": 12,
      "value": "#[Error_Msg] - \u0027Error in Cart\u0027"
    },
    {
      "line": 13,
      "value": "#[Due_date_message] - \u0027Your Next Due Date is Date\u0027"
    },
    {
      "line": 14,
      "value": "#[OS/SD/PM on submitorder] - order summary, shipping detail , payment method on submit-order-page"
    },
    {
      "line": 15,
      "value": "#[Paypal_Account_Detail] - username,id,password for new paypal account creation"
    },
    {
      "line": 16,
      "value": "#[CreditCardInformation] - nameOfCard, cardNumber, exp-date, cvv, firstName, lastName, address"
    },
    {
      "line": 17,
      "value": "#[OrderInformationMoreThan1000$] - ItemId , Quantity ,Payment Information of item more than 1000$"
    },
    {
      "line": 18,
      "value": "# [Email-idErrorMessage] - error message for no email id"
    },
    {
      "line": 19,
      "value": "#[PaymentInformation] - details for making payment"
    },
    {
      "line": 20,
      "value": "#[PaymentMethodUpdatedMessage] - message for updatde payment method in wallet of paypal"
    },
    {
      "line": 21,
      "value": "#[ShippingInformation] - shipping address details"
    },
    {
      "line": 22,
      "value": "#[DetailsOfCheck] - check details that need to be verified"
    },
    {
      "line": 23,
      "value": "#[DetailsOfPurchaseOrder] - details we need to verify for purchase order"
    },
    {
      "line": 24,
      "value": "#[PaypalDetailInformation] details that need to be verified for paypal"
    },
    {
      "line": 25,
      "value": "#[Welcome Information] - message for becoming red apple user"
    },
    {
      "line": 26,
      "value": "#[Free_shipping_Msg] - message for free shipping"
    },
    {
      "line": 27,
      "value": "#[Express_Shipping_Msg] - message for express shipping"
    },
    {
      "line": 28,
      "value": "#[SchoolInformation] - information of new school to be edited with current"
    },
    {
      "line": 29,
      "value": "#[Insufficient_BP_Balance] - message for insufficient bonus point"
    },
    {
      "line": 30,
      "value": "#[InvalidCreditCardDetails] - details of invalid credit card"
    },
    {
      "line": 31,
      "value": "#[OrderInformationMoreThan1000$] - ItemId , Quantity that makes order total 1000$"
    },
    {
      "line": 32,
      "value": "#[NewMasterCreditCardDetails] - credit card details for master card"
    }
  ],
  "line": 34,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 35,
  "name": "Parent opens scholastic reading clubs e-commerce site",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginSteps.Parent_opens_scholastic_reading_clubs_ecommerce_site()"
});
formatter.result({
  "duration": 6724559426,
  "status": "passed"
});
formatter.scenario({
  "line": 50,
  "name": "Pop up seen when clicked on the Reading Level in PDP page:[InSprint13.1:PCOD:Parent_PDPPage_ReadingLevelWindowPopup]",
  "description": "",
  "id": "auto-toss-in;pop-up-seen-when-clicked-on-the-reading-level-in-pdp-page:[\u003cusingdata\u003e];;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@P1"
    },
    {
      "line": 1,
      "name": "@Android"
    },
    {
      "line": 1,
      "name": "@IOS"
    },
    {
      "line": 1,
      "name": "@Automatic_Q_A_Toss-in"
    },
    {
      "line": 1,
      "name": "@Regression"
    },
    {
      "line": 1,
      "name": "@Web"
    }
  ]
});
formatter.step({
  "line": 41,
  "name": "Parent login in with valid [username] and [password]",
  "keyword": "Given "
});
formatter.step({
  "line": 42,
  "name": "parent searches with valid item which is having reading level information",
  "keyword": "And "
});
formatter.step({
  "line": 43,
  "name": "PDP of the item displays",
  "keyword": "And "
});
formatter.step({
  "line": 44,
  "name": "parent click on Reading Level line",
  "keyword": "When "
});
formatter.step({
  "line": 45,
  "name": "pop up is displayed with reading level infomation",
  "keyword": "Then "
});
formatter.step({
  "line": 46,
  "name": "“About Reading Levels” link is present in the pop up",
  "keyword": "And "
});
formatter.match({
  "location": "PCOD_LoginSteps.parent_login_in_with_valid_username_and_password()"
});
formatter.result({
  "duration": 57724779159,
  "status": "passed"
});
formatter.match({
  "location": "PCOD_HomePageSteps.parent_searches_with_valid_item_which_is_having_reading_level_information()"
});
formatter.result({
  "duration": 14209323471,
  "status": "passed"
});
formatter.match({
  "location": "PCOD_HomePageSteps.pdp_of_the_item_displays()"
});
formatter.result({
  "duration": 4141584734,
  "status": "passed"
});
formatter.match({
  "location": "PCOD_HomePageSteps.parent_click_on_reading_level_line()"
});
formatter.result({
  "duration": 32263672053,
  "error_message": "org.openqa.selenium.TimeoutException: Timed out after 30 seconds waiting for DefaultElementLocator \u0027By.cssSelector: .attribute.tooltipstered\u0027 to be present\nBuild info: version: \u00272.53.1\u0027, revision: \u0027a36b8b1cd5757287168e54b817830adce9b0158d\u0027, time: \u00272016-06-30 19:26:09\u0027\nSystem info: host: \u0027RLE0302\u0027, ip: \u002710.32.199.163\u0027, os.name: \u0027Windows 7\u0027, os.arch: \u0027amd64\u0027, os.version: \u00276.1\u0027, java.version: \u00271.8.0_111\u0027\nDriver info: driver.version: unknown\r\n\tat org.openqa.selenium.support.ui.FluentWait.timeoutException(FluentWait.java:292)\r\n\tat org.openqa.selenium.support.ui.FluentWait.until(FluentWait.java:261)\r\n\tat com.scholastic.torque.common.WaitUtils.waitForPresent(WaitUtils.java:29)\r\n\tat com.scholastic.torque.webdriver.ExtendedElementHandler.invoke(ExtendedElementHandler.java:46)\r\n\tat com.sun.proxy.$Proxy23.click(Unknown Source)\r\n\tat com.dw.automation.pages.impl.PCOD_LoginPage.pcod_click_element(PCOD_LoginPage.java:2004)\r\n\tat com.dw.automation.steps.home.PCOD_HomePageSteps.parent_click_on_reading_level_line(PCOD_HomePageSteps.java:263)\r\n\tat ✽.When parent click on Reading Level line(features/testfeature.feature:44)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "PCOD_HomePageSteps.pop_up_is_displayed_with_reading_level_infomation()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "PCOD_HomePageSteps.about_reading_levels_link_is_present_in_the_pop_up()"
});
formatter.result({
  "status": "skipped"
});
formatter.write("Current Page URL is https://dev36-rco-scholastic.demandware.net/s/rco-ca/100-ant-picnic-math-activity-set/2667999-rco-ca.html");
formatter.embedding("image/png", "embedded0.png");
formatter.after({
  "duration": 1067081406,
  "status": "passed"
});
});