$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("features/testfeature.feature");
formatter.feature({
  "line": 2,
  "name": "Auto Toss-in",
  "description": "This feature is used to check for the Automatic Toss-in features",
  "id": "auto-toss-in",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@Automatic_Q_A_Toss-in"
    },
    {
      "line": 1,
      "name": "@P1"
    },
    {
      "line": 1,
      "name": "@Regression"
    },
    {
      "line": 1,
      "name": "@Android"
    },
    {
      "line": 1,
      "name": "@IOS"
    },
    {
      "line": 1,
      "name": "@Web"
    }
  ]
});
formatter.scenarioOutline({
  "comments": [
    {
      "line": 37,
      "value": "########################## Teacher Checkout Test Case ################################"
    }
  ],
  "line": 39,
  "name": "Parent enters Visa card information with country selected as US and proceeds to next step:[\u003cUsingData\u003e]",
  "description": "",
  "id": "auto-toss-in;parent-enters-visa-card-information-with-country-selected-as-us-and-proceeds-to-next-step:[\u003cusingdata\u003e]",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 41,
  "name": "Parent login in with valid [username] and [password]",
  "keyword": "Given "
});
formatter.step({
  "line": 42,
  "name": "parent add items to cart and navigate to shopping cart",
  "keyword": "And "
});
formatter.step({
  "line": 43,
  "name": "parent navigates to Shipping \u0026 Payment page",
  "keyword": "And "
});
formatter.step({
  "line": 44,
  "name": "parent selects “Add a new Card” from credit card dropdown",
  "keyword": "When "
});
formatter.step({
  "line": 45,
  "name": "enters valid data in all credit card fields entering Visa card number and country selected as US",
  "keyword": "And "
});
formatter.step({
  "line": 46,
  "name": "clicks on Continue Checkout",
  "keyword": "And "
});
formatter.step({
  "line": 47,
  "name": "Submit Your Order page displays",
  "keyword": "Then "
});
formatter.examples({
  "line": 49,
  "name": "",
  "description": "",
  "id": "auto-toss-in;parent-enters-visa-card-information-with-country-selected-as-us-and-proceeds-to-next-step:[\u003cusingdata\u003e];",
  "rows": [
    {
      "cells": [
        "UsingData"
      ],
      "line": 50,
      "id": "auto-toss-in;parent-enters-visa-card-information-with-country-selected-as-us-and-proceeds-to-next-step:[\u003cusingdata\u003e];;1"
    },
    {
      "cells": [
        "InSprint13.4:ParentOrder_CreditCard_AddNewCard_US_Visa"
      ],
      "line": 51,
      "id": "auto-toss-in;parent-enters-visa-card-information-with-country-selected-as-us-and-proceeds-to-next-step:[\u003cusingdata\u003e];;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 30885278536,
  "status": "passed"
});
formatter.background({
  "comments": [
    {
      "line": 5,
      "value": "#[UserInformation]- UserName,Password"
    },
    {
      "line": 6,
      "value": "#[ItemInformation]-StudentName,ItemId"
    },
    {
      "line": 7,
      "value": "#[OrderSummary]-Bonus Points Used ,Bonus Points Earned ,Student Online Orders (Paid) ,Student Flyer Orders,Your Teacher Order"
    },
    {
      "line": 8,
      "value": "#[OrderInformation]-ItemId , Quantity and other attributes related to item."
    },
    {
      "line": 9,
      "value": "#[Free_Shipping_msg]- \u0027shipping fee is $3.50\u0027"
    },
    {
      "line": 10,
      "value": "#[Shipping_Charge_Msg]- \u0027Shipping charge as $3.50\u0027"
    },
    {
      "line": 11,
      "value": "#[Due_date_message] - \u0027Due Day message\u0027"
    },
    {
      "line": 12,
      "value": "#[Error_Msg] - \u0027Error in Cart\u0027"
    },
    {
      "line": 13,
      "value": "#[Due_date_message] - \u0027Your Next Due Date is Date\u0027"
    },
    {
      "line": 14,
      "value": "#[OS/SD/PM on submitorder] - order summary, shipping detail , payment method on submit-order-page"
    },
    {
      "line": 15,
      "value": "#[Paypal_Account_Detail] - username,id,password for new paypal account creation"
    },
    {
      "line": 16,
      "value": "#[CreditCardInformation] - nameOfCard, cardNumber, exp-date, cvv, firstName, lastName, address"
    },
    {
      "line": 17,
      "value": "#[OrderInformationMoreThan1000$] - ItemId , Quantity ,Payment Information of item more than 1000$"
    },
    {
      "line": 18,
      "value": "# [Email-idErrorMessage] - error message for no email id"
    },
    {
      "line": 19,
      "value": "#[PaymentInformation] - details for making payment"
    },
    {
      "line": 20,
      "value": "#[PaymentMethodUpdatedMessage] - message for updatde payment method in wallet of paypal"
    },
    {
      "line": 21,
      "value": "#[ShippingInformation] - shipping address details"
    },
    {
      "line": 22,
      "value": "#[DetailsOfCheck] - check details that need to be verified"
    },
    {
      "line": 23,
      "value": "#[DetailsOfPurchaseOrder] - details we need to verify for purchase order"
    },
    {
      "line": 24,
      "value": "#[PaypalDetailInformation] details that need to be verified for paypal"
    },
    {
      "line": 25,
      "value": "#[Welcome Information] - message for becoming red apple user"
    },
    {
      "line": 26,
      "value": "#[Free_shipping_Msg] - message for free shipping"
    },
    {
      "line": 27,
      "value": "#[Express_Shipping_Msg] - message for express shipping"
    },
    {
      "line": 28,
      "value": "#[SchoolInformation] - information of new school to be edited with current"
    },
    {
      "line": 29,
      "value": "#[Insufficient_BP_Balance] - message for insufficient bonus point"
    },
    {
      "line": 30,
      "value": "#[InvalidCreditCardDetails] - details of invalid credit card"
    },
    {
      "line": 31,
      "value": "#[OrderInformationMoreThan1000$] - ItemId , Quantity that makes order total 1000$"
    },
    {
      "line": 32,
      "value": "#[NewMasterCreditCardDetails] - credit card details for master card"
    }
  ],
  "line": 34,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 35,
  "name": "Parent opens scholastic reading clubs e-commerce site",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginSteps.Parent_opens_scholastic_reading_clubs_ecommerce_site()"
});
formatter.result({
  "duration": 6507214383,
  "status": "passed"
});
formatter.scenario({
  "line": 51,
  "name": "Parent enters Visa card information with country selected as US and proceeds to next step:[InSprint13.4:ParentOrder_CreditCard_AddNewCard_US_Visa]",
  "description": "",
  "id": "auto-toss-in;parent-enters-visa-card-information-with-country-selected-as-us-and-proceeds-to-next-step:[\u003cusingdata\u003e];;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@P1"
    },
    {
      "line": 1,
      "name": "@Android"
    },
    {
      "line": 1,
      "name": "@IOS"
    },
    {
      "line": 1,
      "name": "@Automatic_Q_A_Toss-in"
    },
    {
      "line": 1,
      "name": "@Regression"
    },
    {
      "line": 1,
      "name": "@Web"
    }
  ]
});
formatter.step({
  "line": 41,
  "name": "Parent login in with valid [username] and [password]",
  "keyword": "Given "
});
formatter.step({
  "line": 42,
  "name": "parent add items to cart and navigate to shopping cart",
  "keyword": "And "
});
formatter.step({
  "line": 43,
  "name": "parent navigates to Shipping \u0026 Payment page",
  "keyword": "And "
});
formatter.step({
  "line": 44,
  "name": "parent selects “Add a new Card” from credit card dropdown",
  "keyword": "When "
});
formatter.step({
  "line": 45,
  "name": "enters valid data in all credit card fields entering Visa card number and country selected as US",
  "keyword": "And "
});
formatter.step({
  "line": 46,
  "name": "clicks on Continue Checkout",
  "keyword": "And "
});
formatter.step({
  "line": 47,
  "name": "Submit Your Order page displays",
  "keyword": "Then "
});
formatter.match({
  "location": "PCOD_LoginSteps.parent_login_in_with_valid_username_and_password()"
});
formatter.result({
  "duration": 126588699489,
  "status": "passed"
});
formatter.match({
  "location": "PCOD_PDPPageSteps.parent_add_items_to_cart_and_navigate_to_shopping_cart()"
});
formatter.result({
  "duration": 17209043544,
  "status": "passed"
});
formatter.match({
  "location": "PCOD_ReviewCartPageSteps.parent_navigates_to_shipping_payment_page()"
});
formatter.result({
  "duration": 8132659916,
  "status": "passed"
});
formatter.match({
  "location": "PCOD_ShippingAndPaymentPageSteps.parent_selects_add_a_new_card_from_credit_card_dropdown()"
});
formatter.result({
  "duration": 14987582902,
  "status": "passed"
});
formatter.match({
  "location": "PCOD_ShippingAndPaymentPageSteps.enters_valid_data_in_all_credit_card_fields_entering_visa_card_number_and_country_selected_as_us()"
});
formatter.result({
  "duration": 21440467348,
  "status": "passed"
});
formatter.match({
  "location": "PCOD_ShippingAndPaymentPageSteps.clicks_on_continue_checkout()"
});
formatter.result({
  "duration": 16470754319,
  "status": "passed"
});
formatter.match({
  "location": "PCOD_SubmitYourOrderPageSteps.submit_your_order_page_displays()"
});
formatter.result({
  "duration": 2748172145,
  "error_message": "java.lang.AssertionError: expected [true] but found [false]\r\n\tat org.testng.Assert.fail(Assert.java:94)\r\n\tat org.testng.Assert.failNotEquals(Assert.java:496)\r\n\tat org.testng.Assert.assertTrue(Assert.java:42)\r\n\tat org.testng.Assert.assertTrue(Assert.java:52)\r\n\tat com.dw.automation.steps.home.PCOD_SubmitYourOrderPageSteps.submit_your_order_page_displays(PCOD_SubmitYourOrderPageSteps.java:63)\r\n\tat ✽.Then Submit Your Order page displays(features/testfeature.feature:47)\r\n",
  "status": "failed"
});
formatter.write("Current Page URL is https://dev36-rco-scholastic.demandware.net/s/rco-ca/checkout?dwcont\u003dC1125930836");
formatter.embedding("image/png", "embedded0.png");
formatter.after({
  "duration": 1049870399,
  "status": "passed"
});
});