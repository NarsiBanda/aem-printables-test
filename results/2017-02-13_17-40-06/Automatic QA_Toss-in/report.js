$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("features/testfeature.feature");
formatter.feature({
  "line": 2,
  "name": "Auto Toss-in",
  "description": "This feature is used to check for the Automatic Toss-in features",
  "id": "auto-toss-in",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@Automatic_Q_A_Toss-in"
    },
    {
      "line": 1,
      "name": "@P1"
    },
    {
      "line": 1,
      "name": "@Regression"
    },
    {
      "line": 1,
      "name": "@Android"
    },
    {
      "line": 1,
      "name": "@IOS"
    },
    {
      "line": 1,
      "name": "@Web"
    }
  ]
});
formatter.scenarioOutline({
  "comments": [
    {
      "line": 37,
      "value": "########################## Teacher Checkout Test Case ################################"
    }
  ],
  "line": 39,
  "name": "Order of Children in child dropdown:[\u003cUsingData\u003e]",
  "description": "",
  "id": "auto-toss-in;order-of-children-in-child-dropdown:[\u003cusingdata\u003e]",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 41,
  "name": "Parent login in with valid [username] and [password]",
  "keyword": "Given "
});
formatter.step({
  "line": 42,
  "name": "parent add item from PDP page and navigates to shopping cart page",
  "keyword": "And "
});
formatter.step({
  "line": 43,
  "name": "the items are assigned to the default child",
  "keyword": "And "
});
formatter.step({
  "line": 44,
  "name": "parent clicks on child dropdown",
  "keyword": "When "
});
formatter.step({
  "line": 45,
  "name": "all the children are listed in the dropdown",
  "keyword": "Then "
});
formatter.step({
  "line": 46,
  "name": "the order in which the children are displayed are based on highest to lowest grade of the child",
  "keyword": "And "
});
formatter.examples({
  "line": 48,
  "name": "",
  "description": "",
  "id": "auto-toss-in;order-of-children-in-child-dropdown:[\u003cusingdata\u003e];",
  "rows": [
    {
      "cells": [
        "UsingData"
      ],
      "line": 49,
      "id": "auto-toss-in;order-of-children-in-child-dropdown:[\u003cusingdata\u003e];;1"
    },
    {
      "cells": [
        "InSprint13.4:PCOD:ParentOrder_ChildOrder_Dropdown"
      ],
      "line": 50,
      "id": "auto-toss-in;order-of-children-in-child-dropdown:[\u003cusingdata\u003e];;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 27510100932,
  "status": "passed"
});
formatter.background({
  "comments": [
    {
      "line": 5,
      "value": "#[UserInformation]- UserName,Password"
    },
    {
      "line": 6,
      "value": "#[ItemInformation]-StudentName,ItemId"
    },
    {
      "line": 7,
      "value": "#[OrderSummary]-Bonus Points Used ,Bonus Points Earned ,Student Online Orders (Paid) ,Student Flyer Orders,Your Teacher Order"
    },
    {
      "line": 8,
      "value": "#[OrderInformation]-ItemId , Quantity and other attributes related to item."
    },
    {
      "line": 9,
      "value": "#[Free_Shipping_msg]- \u0027shipping fee is $3.50\u0027"
    },
    {
      "line": 10,
      "value": "#[Shipping_Charge_Msg]- \u0027Shipping charge as $3.50\u0027"
    },
    {
      "line": 11,
      "value": "#[Due_date_message] - \u0027Due Day message\u0027"
    },
    {
      "line": 12,
      "value": "#[Error_Msg] - \u0027Error in Cart\u0027"
    },
    {
      "line": 13,
      "value": "#[Due_date_message] - \u0027Your Next Due Date is Date\u0027"
    },
    {
      "line": 14,
      "value": "#[OS/SD/PM on submitorder] - order summary, shipping detail , payment method on submit-order-page"
    },
    {
      "line": 15,
      "value": "#[Paypal_Account_Detail] - username,id,password for new paypal account creation"
    },
    {
      "line": 16,
      "value": "#[CreditCardInformation] - nameOfCard, cardNumber, exp-date, cvv, firstName, lastName, address"
    },
    {
      "line": 17,
      "value": "#[OrderInformationMoreThan1000$] - ItemId , Quantity ,Payment Information of item more than 1000$"
    },
    {
      "line": 18,
      "value": "# [Email-idErrorMessage] - error message for no email id"
    },
    {
      "line": 19,
      "value": "#[PaymentInformation] - details for making payment"
    },
    {
      "line": 20,
      "value": "#[PaymentMethodUpdatedMessage] - message for updatde payment method in wallet of paypal"
    },
    {
      "line": 21,
      "value": "#[ShippingInformation] - shipping address details"
    },
    {
      "line": 22,
      "value": "#[DetailsOfCheck] - check details that need to be verified"
    },
    {
      "line": 23,
      "value": "#[DetailsOfPurchaseOrder] - details we need to verify for purchase order"
    },
    {
      "line": 24,
      "value": "#[PaypalDetailInformation] details that need to be verified for paypal"
    },
    {
      "line": 25,
      "value": "#[Welcome Information] - message for becoming red apple user"
    },
    {
      "line": 26,
      "value": "#[Free_shipping_Msg] - message for free shipping"
    },
    {
      "line": 27,
      "value": "#[Express_Shipping_Msg] - message for express shipping"
    },
    {
      "line": 28,
      "value": "#[SchoolInformation] - information of new school to be edited with current"
    },
    {
      "line": 29,
      "value": "#[Insufficient_BP_Balance] - message for insufficient bonus point"
    },
    {
      "line": 30,
      "value": "#[InvalidCreditCardDetails] - details of invalid credit card"
    },
    {
      "line": 31,
      "value": "#[OrderInformationMoreThan1000$] - ItemId , Quantity that makes order total 1000$"
    },
    {
      "line": 32,
      "value": "#[NewMasterCreditCardDetails] - credit card details for master card"
    }
  ],
  "line": 34,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 35,
  "name": "Parent opens scholastic reading clubs e-commerce site",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginSteps.Parent_opens_scholastic_reading_clubs_ecommerce_site()"
});
formatter.result({
  "duration": 6566307211,
  "status": "passed"
});
formatter.scenario({
  "line": 50,
  "name": "Order of Children in child dropdown:[InSprint13.4:PCOD:ParentOrder_ChildOrder_Dropdown]",
  "description": "",
  "id": "auto-toss-in;order-of-children-in-child-dropdown:[\u003cusingdata\u003e];;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@P1"
    },
    {
      "line": 1,
      "name": "@Android"
    },
    {
      "line": 1,
      "name": "@IOS"
    },
    {
      "line": 1,
      "name": "@Automatic_Q_A_Toss-in"
    },
    {
      "line": 1,
      "name": "@Regression"
    },
    {
      "line": 1,
      "name": "@Web"
    }
  ]
});
formatter.step({
  "line": 41,
  "name": "Parent login in with valid [username] and [password]",
  "keyword": "Given "
});
formatter.step({
  "line": 42,
  "name": "parent add item from PDP page and navigates to shopping cart page",
  "keyword": "And "
});
formatter.step({
  "line": 43,
  "name": "the items are assigned to the default child",
  "keyword": "And "
});
formatter.step({
  "line": 44,
  "name": "parent clicks on child dropdown",
  "keyword": "When "
});
formatter.step({
  "line": 45,
  "name": "all the children are listed in the dropdown",
  "keyword": "Then "
});
formatter.step({
  "line": 46,
  "name": "the order in which the children are displayed are based on highest to lowest grade of the child",
  "keyword": "And "
});
formatter.match({
  "location": "PCOD_LoginSteps.parent_login_in_with_valid_username_and_password()"
});
formatter.result({
  "duration": 55505922282,
  "status": "passed"
});
formatter.match({
  "location": "PCOD_PDPPageSteps.parent_add_item_from_pdp_page_and_navigates_to_shopping_cart_page()"
});
formatter.result({
  "duration": 129185826085,
  "status": "passed"
});
formatter.match({
  "location": "PCOD_ReviewCartPageSteps.the_items_are_assigned_to_the_default_child()"
});
formatter.result({
  "duration": 5280553204,
  "status": "passed"
});
formatter.match({
  "location": "PCOD_ReviewCartPageSteps.parent_clicks_on_child_dropdown()"
});
formatter.result({
  "duration": 3239325594,
  "status": "passed"
});
formatter.match({
  "location": "PCOD_ReviewCartPageSteps.all_the_children_are_listed_in_the_dropdown()"
});
formatter.result({
  "duration": 2729696783,
  "error_message": "java.lang.AssertionError: Child names not present in dropdown i.e. testchild1 test\r\n\tat org.testng.Assert.fail(Assert.java:94)\r\n\tat com.dw.automation.pages.impl.PCOD_ReviewCartPage.verify_childs_childdropdown_reviewcart(PCOD_ReviewCartPage.java:527)\r\n\tat com.dw.automation.steps.home.PCOD_ReviewCartPageSteps.all_the_children_are_listed_in_the_dropdown(PCOD_ReviewCartPageSteps.java:672)\r\n\tat ✽.Then all the children are listed in the dropdown(features/testfeature.feature:45)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "PCOD_ReviewCartPageSteps.the_order_in_which_the_children_are_displayed_are_based_on_highest_to_lowest_grade_of_the_child()"
});
formatter.result({
  "status": "skipped"
});
formatter.write("Current Page URL is https://dev36-rco-scholastic.demandware.net/s/rco-ca/checkout");
formatter.embedding("image/png", "embedded0.png");
formatter.after({
  "duration": 722324313,
  "status": "passed"
});
});