$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("features/testfeature.feature");
formatter.feature({
  "line": 2,
  "name": "Auto Toss-in",
  "description": "This feature is used to check for the Automatic Toss-in features",
  "id": "auto-toss-in",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@Automatic_Q_A_Toss-in"
    },
    {
      "line": 1,
      "name": "@P1"
    },
    {
      "line": 1,
      "name": "@Regression"
    },
    {
      "line": 1,
      "name": "@Android"
    },
    {
      "line": 1,
      "name": "@IOS"
    },
    {
      "line": 1,
      "name": "@Web"
    }
  ]
});
formatter.scenarioOutline({
  "comments": [
    {
      "line": 37,
      "value": "########################## Teacher Checkout Test Case ################################"
    }
  ],
  "line": 39,
  "name": "Shipping section is updated when parent changes the child assignment for items:[\u003cUsingData\u003e]",
  "description": "",
  "id": "auto-toss-in;shipping-section-is-updated-when-parent-changes-the-child-assignment-for-items:[\u003cusingdata\u003e]",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 41,
  "name": "parent has 2 childs connected to different teacher",
  "keyword": "Given "
});
formatter.step({
  "line": 42,
  "name": "Parent login in with valid [username] and [password]",
  "keyword": "And "
});
formatter.step({
  "line": 43,
  "name": "parent add items to cart",
  "keyword": "And "
});
formatter.step({
  "line": 44,
  "name": "the items are assigned to the (default child) Child1",
  "keyword": "And "
});
formatter.step({
  "line": 45,
  "name": "parent navigates to Submit Your Order page page",
  "keyword": "And "
});
formatter.step({
  "line": 46,
  "name": "Shipping section displays with  “\u003cChild1name\u003e\u0027s items will be shipped to:”",
  "keyword": "And "
});
formatter.step({
  "line": 47,
  "name": "teacher1\u0027s first and last name with school address \u003cSchool Name1, Street, city,Country, postal code\u003e displays",
  "keyword": "And "
});
formatter.step({
  "line": 48,
  "name": "parent navigates to Shopping cart page from submit your order page",
  "keyword": "When "
});
formatter.step({
  "line": 49,
  "name": "reassigns few items to child2",
  "keyword": "And "
});
formatter.step({
  "line": 50,
  "name": "parent navigates to Submit Your Order page page",
  "keyword": "And "
});
formatter.step({
  "line": 51,
  "name": "Shipping section displays with “\u003cChild1name\u003e\u0027s items will be shipped to:”",
  "keyword": "Then "
});
formatter.step({
  "line": 52,
  "name": "teacher1\u0027s first and last name with school address \u003cSchool Name1, Street, city,Country, postal code\u003e displays",
  "keyword": "And "
});
formatter.step({
  "line": 53,
  "name": "“\u003cChild2name\u003e\u0027s items will be shipped to:”",
  "keyword": "And "
});
formatter.step({
  "line": 54,
  "name": "teacher2\u0027s first and last name with school address \u003cSchool Name1, Street, city,Country, postal code\u003e displays",
  "keyword": "And "
});
formatter.examples({
  "line": 56,
  "name": "",
  "description": "",
  "id": "auto-toss-in;shipping-section-is-updated-when-parent-changes-the-child-assignment-for-items:[\u003cusingdata\u003e];",
  "rows": [
    {
      "cells": [
        "UsingData"
      ],
      "line": 57,
      "id": "auto-toss-in;shipping-section-is-updated-when-parent-changes-the-child-assignment-for-items:[\u003cusingdata\u003e];;1"
    },
    {
      "cells": [
        "InSprint13.4:PCOD:ParentOrder_SubmitYourOrder_ShippingSection_1Child_DifferentTeacher"
      ],
      "line": 58,
      "id": "auto-toss-in;shipping-section-is-updated-when-parent-changes-the-child-assignment-for-items:[\u003cusingdata\u003e];;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 31571801923,
  "status": "passed"
});
formatter.background({
  "comments": [
    {
      "line": 5,
      "value": "#[UserInformation]- UserName,Password"
    },
    {
      "line": 6,
      "value": "#[ItemInformation]-StudentName,ItemId"
    },
    {
      "line": 7,
      "value": "#[OrderSummary]-Bonus Points Used ,Bonus Points Earned ,Student Online Orders (Paid) ,Student Flyer Orders,Your Teacher Order"
    },
    {
      "line": 8,
      "value": "#[OrderInformation]-ItemId , Quantity and other attributes related to item."
    },
    {
      "line": 9,
      "value": "#[Free_Shipping_msg]- \u0027shipping fee is $3.50\u0027"
    },
    {
      "line": 10,
      "value": "#[Shipping_Charge_Msg]- \u0027Shipping charge as $3.50\u0027"
    },
    {
      "line": 11,
      "value": "#[Due_date_message] - \u0027Due Day message\u0027"
    },
    {
      "line": 12,
      "value": "#[Error_Msg] - \u0027Error in Cart\u0027"
    },
    {
      "line": 13,
      "value": "#[Due_date_message] - \u0027Your Next Due Date is Date\u0027"
    },
    {
      "line": 14,
      "value": "#[OS/SD/PM on submitorder] - order summary, shipping detail , payment method on submit-order-page"
    },
    {
      "line": 15,
      "value": "#[Paypal_Account_Detail] - username,id,password for new paypal account creation"
    },
    {
      "line": 16,
      "value": "#[CreditCardInformation] - nameOfCard, cardNumber, exp-date, cvv, firstName, lastName, address"
    },
    {
      "line": 17,
      "value": "#[OrderInformationMoreThan1000$] - ItemId , Quantity ,Payment Information of item more than 1000$"
    },
    {
      "line": 18,
      "value": "# [Email-idErrorMessage] - error message for no email id"
    },
    {
      "line": 19,
      "value": "#[PaymentInformation] - details for making payment"
    },
    {
      "line": 20,
      "value": "#[PaymentMethodUpdatedMessage] - message for updatde payment method in wallet of paypal"
    },
    {
      "line": 21,
      "value": "#[ShippingInformation] - shipping address details"
    },
    {
      "line": 22,
      "value": "#[DetailsOfCheck] - check details that need to be verified"
    },
    {
      "line": 23,
      "value": "#[DetailsOfPurchaseOrder] - details we need to verify for purchase order"
    },
    {
      "line": 24,
      "value": "#[PaypalDetailInformation] details that need to be verified for paypal"
    },
    {
      "line": 25,
      "value": "#[Welcome Information] - message for becoming red apple user"
    },
    {
      "line": 26,
      "value": "#[Free_shipping_Msg] - message for free shipping"
    },
    {
      "line": 27,
      "value": "#[Express_Shipping_Msg] - message for express shipping"
    },
    {
      "line": 28,
      "value": "#[SchoolInformation] - information of new school to be edited with current"
    },
    {
      "line": 29,
      "value": "#[Insufficient_BP_Balance] - message for insufficient bonus point"
    },
    {
      "line": 30,
      "value": "#[InvalidCreditCardDetails] - details of invalid credit card"
    },
    {
      "line": 31,
      "value": "#[OrderInformationMoreThan1000$] - ItemId , Quantity that makes order total 1000$"
    },
    {
      "line": 32,
      "value": "#[NewMasterCreditCardDetails] - credit card details for master card"
    }
  ],
  "line": 34,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 35,
  "name": "Parent opens scholastic reading clubs e-commerce site",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginSteps.Parent_opens_scholastic_reading_clubs_ecommerce_site()"
});
formatter.result({
  "duration": 6531496164,
  "status": "passed"
});
formatter.scenario({
  "line": 58,
  "name": "Shipping section is updated when parent changes the child assignment for items:[InSprint13.4:PCOD:ParentOrder_SubmitYourOrder_ShippingSection_1Child_DifferentTeacher]",
  "description": "",
  "id": "auto-toss-in;shipping-section-is-updated-when-parent-changes-the-child-assignment-for-items:[\u003cusingdata\u003e];;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@P1"
    },
    {
      "line": 1,
      "name": "@Android"
    },
    {
      "line": 1,
      "name": "@IOS"
    },
    {
      "line": 1,
      "name": "@Automatic_Q_A_Toss-in"
    },
    {
      "line": 1,
      "name": "@Regression"
    },
    {
      "line": 1,
      "name": "@Web"
    }
  ]
});
formatter.step({
  "line": 41,
  "name": "parent has 2 childs connected to different teacher",
  "keyword": "Given "
});
formatter.step({
  "line": 42,
  "name": "Parent login in with valid [username] and [password]",
  "keyword": "And "
});
formatter.step({
  "line": 43,
  "name": "parent add items to cart",
  "keyword": "And "
});
formatter.step({
  "line": 44,
  "name": "the items are assigned to the (default child) Child1",
  "keyword": "And "
});
formatter.step({
  "line": 45,
  "name": "parent navigates to Submit Your Order page page",
  "keyword": "And "
});
formatter.step({
  "line": 46,
  "name": "Shipping section displays with  “\u003cChild1name\u003e\u0027s items will be shipped to:”",
  "keyword": "And "
});
formatter.step({
  "line": 47,
  "name": "teacher1\u0027s first and last name with school address \u003cSchool Name1, Street, city,Country, postal code\u003e displays",
  "keyword": "And "
});
formatter.step({
  "line": 48,
  "name": "parent navigates to Shopping cart page from submit your order page",
  "keyword": "When "
});
formatter.step({
  "line": 49,
  "name": "reassigns few items to child2",
  "keyword": "And "
});
formatter.step({
  "line": 50,
  "name": "parent navigates to Submit Your Order page page",
  "keyword": "And "
});
formatter.step({
  "line": 51,
  "name": "Shipping section displays with “\u003cChild1name\u003e\u0027s items will be shipped to:”",
  "keyword": "Then "
});
formatter.step({
  "line": 52,
  "name": "teacher1\u0027s first and last name with school address \u003cSchool Name1, Street, city,Country, postal code\u003e displays",
  "keyword": "And "
});
formatter.step({
  "line": 53,
  "name": "“\u003cChild2name\u003e\u0027s items will be shipped to:”",
  "keyword": "And "
});
formatter.step({
  "line": 54,
  "name": "teacher2\u0027s first and last name with school address \u003cSchool Name1, Street, city,Country, postal code\u003e displays",
  "keyword": "And "
});
formatter.match({
  "location": "PCOD_LoginSteps.parent_has_2_childs_connected_to_different_teacher()"
});
formatter.result({
  "duration": 12121679,
  "status": "passed"
});
formatter.match({
  "location": "PCOD_LoginSteps.parent_login_in_with_valid_username_and_password()"
});
formatter.result({
  "duration": 61179169728,
  "status": "passed"
});
formatter.match({
  "location": "PCOD_PDPPageSteps.parent_add_items_to_cart()"
});
formatter.result({
  "duration": 18510308535,
  "status": "passed"
});
formatter.match({
  "location": "PCOD_ReviewCartPageSteps.the_items_are_assigned_to_the_default_child_child1()"
});
formatter.result({
  "duration": 5278117484,
  "status": "passed"
});
formatter.match({
  "location": "PCOD_ReviewCartPageSteps.parent_navigates_to_submit_your_order_page_page()"
});
formatter.result({
  "duration": 82122350438,
  "status": "passed"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({
  "arguments": [
    {
      "val": "\u003cSchool Name1, Street, city,Country, postal code\u003e",
      "offset": 51
    }
  ],
  "location": "PCOD_ShippingAndPaymentPageSteps.teacher1s_first_and_last_name_with_school_address_displays(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "PCOD_SubmitYourOrderPageSteps.parent_navigates_to_shopping_cart_page_from_submit_your_order_page()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "PCOD_ReviewCartPageSteps.reassigns_few_items_to_child2()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "PCOD_ReviewCartPageSteps.parent_navigates_to_submit_your_order_page_page()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "\u003cChild1name\u003e",
      "offset": 32
    }
  ],
  "location": "PCOD_ShippingAndPaymentPageSteps.shipping_section_displays_with_s_items_will_be_shipped_to(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "\u003cSchool Name1, Street, city,Country, postal code\u003e",
      "offset": 51
    }
  ],
  "location": "PCOD_ShippingAndPaymentPageSteps.teacher1s_first_and_last_name_with_school_address_displays(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "\u003cChild2name\u003e",
      "offset": 1
    }
  ],
  "location": "PCOD_ShippingAndPaymentPageSteps.s_items_will_be_shipped_to(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "\u003cSchool Name1, Street, city,Country, postal code\u003e",
      "offset": 51
    }
  ],
  "location": "PCOD_ShippingAndPaymentPageSteps.teacher2s_first_and_last_name_with_school_address_displays(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "duration": 710430298,
  "status": "passed"
});
});