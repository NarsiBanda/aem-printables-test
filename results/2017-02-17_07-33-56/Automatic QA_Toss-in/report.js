$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("features/testfeature.feature");
formatter.feature({
  "line": 2,
  "name": "Auto Toss-in",
  "description": "This feature is used to check for the Automatic Toss-in features",
  "id": "auto-toss-in",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@Automatic_Q_A_Toss-in"
    },
    {
      "line": 1,
      "name": "@P1"
    },
    {
      "line": 1,
      "name": "@Regression"
    },
    {
      "line": 1,
      "name": "@Android"
    },
    {
      "line": 1,
      "name": "@IOS"
    },
    {
      "line": 1,
      "name": "@Web"
    }
  ]
});
formatter.scenarioOutline({
  "comments": [
    {
      "line": 37,
      "value": "########################## Teacher Checkout Test Case ################################"
    }
  ],
  "line": 39,
  "name": "Parent changes the child selection and returns back to shopping cart:[\u003cUsingData\u003e]",
  "description": "",
  "id": "auto-toss-in;parent-changes-the-child-selection-and-returns-back-to-shopping-cart:[\u003cusingdata\u003e]",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 41,
  "name": "Parent login in with valid [username] and [password]",
  "keyword": "Given "
});
formatter.step({
  "line": 42,
  "name": "parent add item from PDP page and navigates to shopping cart page",
  "keyword": "And "
});
formatter.step({
  "line": 43,
  "name": "the items are assigned to the default child",
  "keyword": "And "
});
formatter.step({
  "line": 44,
  "name": "parent clicks on child dropdown for few items",
  "keyword": "And "
});
formatter.step({
  "line": 45,
  "name": "reassigns them to a different child",
  "keyword": "And "
});
formatter.step({
  "line": 46,
  "name": "Parent navigates to Home page",
  "keyword": "And "
});
formatter.step({
  "line": 47,
  "name": "Parent navigates back to Shopping cart page",
  "keyword": "When "
});
formatter.step({
  "line": 48,
  "name": "all the items that are reassigned are now assigned to the re-assigned child",
  "keyword": "Then "
});
formatter.step({
  "line": 49,
  "name": "the other items are still assigned to default child",
  "keyword": "And "
});
formatter.examples({
  "line": 51,
  "name": "",
  "description": "",
  "id": "auto-toss-in;parent-changes-the-child-selection-and-returns-back-to-shopping-cart:[\u003cusingdata\u003e];",
  "rows": [
    {
      "cells": [
        "UsingData"
      ],
      "line": 52,
      "id": "auto-toss-in;parent-changes-the-child-selection-and-returns-back-to-shopping-cart:[\u003cusingdata\u003e];;1"
    },
    {
      "cells": [
        "InSprint13.4:PCOD:ParentOrder_ChildNameReassign_BackandForth"
      ],
      "line": 53,
      "id": "auto-toss-in;parent-changes-the-child-selection-and-returns-back-to-shopping-cart:[\u003cusingdata\u003e];;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 60781990796,
  "error_message": "java.lang.RuntimeException: Unable to create local driver\r\n\tat com.scholastic.torque.common.TestBase.initDriver(TestBase.java:158)\r\n\tat com.scholastic.torque.common.TestBase.getDriver(TestBase.java:73)\r\n\tat com.dw.automation.steps.Hooks.beforeHook(Hooks.java:50)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)\r\n\tat java.lang.reflect.Method.invoke(Unknown Source)\r\n\tat cucumber.runtime.Utils$1.call(Utils.java:37)\r\n\tat cucumber.runtime.Timeout.timeout(Timeout.java:13)\r\n\tat cucumber.runtime.Utils.invoke(Utils.java:31)\r\n\tat cucumber.runtime.java.JavaHookDefinition.execute(JavaHookDefinition.java:60)\r\n\tat cucumber.runtime.Runtime.runHookIfTagsMatch(Runtime.java:223)\r\n\tat cucumber.runtime.Runtime.runHooks(Runtime.java:211)\r\n\tat cucumber.runtime.Runtime.runBeforeHooks(Runtime.java:201)\r\n\tat cucumber.runtime.model.CucumberScenario.run(CucumberScenario.java:40)\r\n\tat cucumber.runtime.model.CucumberScenarioOutline.run(CucumberScenarioOutline.java:46)\r\n\tat cucumber.runtime.model.CucumberFeature.run(CucumberFeature.java:165)\r\n\tat com.scholastic.torque.testng.TestNGCucumberRunner.runCucumber(TestNGCucumberRunner.java:147)\r\n\tat com.scholastic.torque.testng.AbstractTestNGCucumberTests.feature(AbstractTestNGCucumberTests.java:30)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)\r\n\tat java.lang.reflect.Method.invoke(Unknown Source)\r\n\tat org.testng.internal.MethodInvocationHelper.invokeMethod(MethodInvocationHelper.java:85)\r\n\tat org.testng.internal.Invoker.invokeMethod(Invoker.java:639)\r\n\tat org.testng.internal.Invoker.invokeTestMethod(Invoker.java:816)\r\n\tat org.testng.internal.Invoker.invokeTestMethods(Invoker.java:1124)\r\n\tat org.testng.internal.TestMethodWorker.invokeTestMethods(TestMethodWorker.java:125)\r\n\tat org.testng.internal.TestMethodWorker.run(TestMethodWorker.java:108)\r\n\tat org.testng.TestRunner.privateRun(TestRunner.java:774)\r\n\tat org.testng.TestRunner.run(TestRunner.java:624)\r\n\tat org.testng.SuiteRunner.runTest(SuiteRunner.java:359)\r\n\tat org.testng.SuiteRunner.runSequentially(SuiteRunner.java:354)\r\n\tat org.testng.SuiteRunner.privateRun(SuiteRunner.java:312)\r\n\tat org.testng.SuiteRunner.run(SuiteRunner.java:261)\r\n\tat org.testng.SuiteRunnerWorker.runSuite(SuiteRunnerWorker.java:52)\r\n\tat org.testng.SuiteRunnerWorker.run(SuiteRunnerWorker.java:86)\r\n\tat org.testng.TestNG.runSuitesSequentially(TestNG.java:1215)\r\n\tat org.testng.TestNG.runSuitesLocally(TestNG.java:1140)\r\n\tat org.testng.TestNG.run(TestNG.java:1048)\r\n\tat org.apache.maven.surefire.testng.TestNGExecutor.run(TestNGExecutor.java:217)\r\n\tat org.apache.maven.surefire.testng.TestNGXmlTestSuite.execute(TestNGXmlTestSuite.java:84)\r\n\tat org.apache.maven.surefire.testng.TestNGProvider.invoke(TestNGProvider.java:92)\r\n\tat org.apache.maven.surefire.booter.ForkedBooter.invokeProviderInSameClassLoader(ForkedBooter.java:200)\r\n\tat org.apache.maven.surefire.booter.ForkedBooter.runSuitesInProcess(ForkedBooter.java:153)\r\n\tat org.apache.maven.surefire.booter.ForkedBooter.main(ForkedBooter.java:103)\r\nCaused by: org.openqa.selenium.WebDriverException: Unalbe to create webdriver\nBuild info: version: \u00272.53.1\u0027, revision: \u0027a36b8b1cd5757287168e54b817830adce9b0158d\u0027, time: \u00272016-06-30 19:26:09\u0027\nSystem info: host: \u0027RLE0302\u0027, ip: \u002710.32.199.63\u0027, os.name: \u0027Windows 7\u0027, os.arch: \u0027amd64\u0027, os.version: \u00276.1\u0027, java.version: \u00271.8.0_111\u0027\nDriver info: driver.version: unknown\r\n\tat com.scholastic.torque.common.TestBase$WEBDRIVER.getDriver(TestBase.java:378)\r\n\tat com.scholastic.torque.common.TestBase.initDriver(TestBase.java:151)\r\n\t... 45 more\r\nCaused by: org.openqa.selenium.remote.UnreachableBrowserException: Could not start a new session. Possible causes are invalid address of the remote server or browser start-up failure.\nBuild info: version: \u00272.53.1\u0027, revision: \u0027a36b8b1cd5757287168e54b817830adce9b0158d\u0027, time: \u00272016-06-30 19:26:09\u0027\nSystem info: host: \u0027RLE0302\u0027, ip: \u002710.32.199.63\u0027, os.name: \u0027Windows 7\u0027, os.arch: \u0027amd64\u0027, os.version: \u00276.1\u0027, java.version: \u00271.8.0_111\u0027\nDriver info: driver.version: ChromeDriver\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:665)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.startSession(RemoteWebDriver.java:249)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.\u003cinit\u003e(RemoteWebDriver.java:131)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.\u003cinit\u003e(RemoteWebDriver.java:144)\r\n\tat org.openqa.selenium.chrome.ChromeDriver.\u003cinit\u003e(ChromeDriver.java:170)\r\n\tat org.openqa.selenium.chrome.ChromeDriver.\u003cinit\u003e(ChromeDriver.java:159)\r\n\tat org.openqa.selenium.chrome.ChromeDriver.\u003cinit\u003e(ChromeDriver.java:116)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat java.lang.Class.newInstance(Unknown Source)\r\n\tat com.scholastic.torque.common.TestBase$WEBDRIVER.getDriver(TestBase.java:376)\r\n\t... 46 more\r\nCaused by: org.openqa.selenium.WebDriverException: java.net.SocketException: Connection reset\nBuild info: version: \u00272.53.1\u0027, revision: \u0027a36b8b1cd5757287168e54b817830adce9b0158d\u0027, time: \u00272016-06-30 19:26:09\u0027\nSystem info: host: \u0027RLE0302\u0027, ip: \u002710.32.199.63\u0027, os.name: \u0027Windows 7\u0027, os.arch: \u0027amd64\u0027, os.version: \u00276.1\u0027, java.version: \u00271.8.0_111\u0027\nDriver info: driver.version: ChromeDriver\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:91)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:644)\r\n\t... 58 more\r\nCaused by: java.net.SocketException: Connection reset\r\n\tat java.net.SocketInputStream.read(Unknown Source)\r\n\tat java.net.SocketInputStream.read(Unknown Source)\r\n\tat org.apache.http.impl.conn.LoggingInputStream.read(LoggingInputStream.java:87)\r\n\tat org.apache.http.impl.io.SessionInputBufferImpl.streamRead(SessionInputBufferImpl.java:136)\r\n\tat org.apache.http.impl.io.SessionInputBufferImpl.fillBuffer(SessionInputBufferImpl.java:152)\r\n\tat org.apache.http.impl.io.SessionInputBufferImpl.readLine(SessionInputBufferImpl.java:270)\r\n\tat org.apache.http.impl.conn.DefaultHttpResponseParser.parseHead(DefaultHttpResponseParser.java:140)\r\n\tat org.apache.http.impl.conn.DefaultHttpResponseParser.parseHead(DefaultHttpResponseParser.java:57)\r\n\tat org.apache.http.impl.io.AbstractMessageParser.parse(AbstractMessageParser.java:260)\r\n\tat org.apache.http.impl.DefaultBHttpClientConnection.receiveResponseHeader(DefaultBHttpClientConnection.java:161)\r\n\tat org.apache.http.impl.conn.CPoolProxy.receiveResponseHeader(CPoolProxy.java:153)\r\n\tat org.apache.http.protocol.HttpRequestExecutor.doReceiveResponse(HttpRequestExecutor.java:271)\r\n\tat org.apache.http.protocol.HttpRequestExecutor.execute(HttpRequestExecutor.java:123)\r\n\tat org.apache.http.impl.execchain.MainClientExec.execute(MainClientExec.java:254)\r\n\tat org.apache.http.impl.execchain.ProtocolExec.execute(ProtocolExec.java:195)\r\n\tat org.apache.http.impl.execchain.RetryExec.execute(RetryExec.java:86)\r\n\tat org.apache.http.impl.execchain.RedirectExec.execute(RedirectExec.java:108)\r\n\tat org.apache.http.impl.client.InternalHttpClient.doExecute(InternalHttpClient.java:184)\r\n\tat org.apache.http.impl.client.CloseableHttpClient.execute(CloseableHttpClient.java:72)\r\n\tat org.apache.http.impl.client.CloseableHttpClient.execute(CloseableHttpClient.java:57)\r\n\tat org.openqa.selenium.remote.internal.ApacheHttpClient.fallBackExecute(ApacheHttpClient.java:144)\r\n\tat org.openqa.selenium.remote.internal.ApacheHttpClient.execute(ApacheHttpClient.java:90)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:142)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:82)\r\n\t... 59 more\r\n",
  "status": "failed"
});
formatter.background({
  "comments": [
    {
      "line": 5,
      "value": "#[UserInformation]- UserName,Password"
    },
    {
      "line": 6,
      "value": "#[ItemInformation]-StudentName,ItemId"
    },
    {
      "line": 7,
      "value": "#[OrderSummary]-Bonus Points Used ,Bonus Points Earned ,Student Online Orders (Paid) ,Student Flyer Orders,Your Teacher Order"
    },
    {
      "line": 8,
      "value": "#[OrderInformation]-ItemId , Quantity and other attributes related to item."
    },
    {
      "line": 9,
      "value": "#[Free_Shipping_msg]- \u0027shipping fee is $3.50\u0027"
    },
    {
      "line": 10,
      "value": "#[Shipping_Charge_Msg]- \u0027Shipping charge as $3.50\u0027"
    },
    {
      "line": 11,
      "value": "#[Due_date_message] - \u0027Due Day message\u0027"
    },
    {
      "line": 12,
      "value": "#[Error_Msg] - \u0027Error in Cart\u0027"
    },
    {
      "line": 13,
      "value": "#[Due_date_message] - \u0027Your Next Due Date is Date\u0027"
    },
    {
      "line": 14,
      "value": "#[OS/SD/PM on submitorder] - order summary, shipping detail , payment method on submit-order-page"
    },
    {
      "line": 15,
      "value": "#[Paypal_Account_Detail] - username,id,password for new paypal account creation"
    },
    {
      "line": 16,
      "value": "#[CreditCardInformation] - nameOfCard, cardNumber, exp-date, cvv, firstName, lastName, address"
    },
    {
      "line": 17,
      "value": "#[OrderInformationMoreThan1000$] - ItemId , Quantity ,Payment Information of item more than 1000$"
    },
    {
      "line": 18,
      "value": "# [Email-idErrorMessage] - error message for no email id"
    },
    {
      "line": 19,
      "value": "#[PaymentInformation] - details for making payment"
    },
    {
      "line": 20,
      "value": "#[PaymentMethodUpdatedMessage] - message for updatde payment method in wallet of paypal"
    },
    {
      "line": 21,
      "value": "#[ShippingInformation] - shipping address details"
    },
    {
      "line": 22,
      "value": "#[DetailsOfCheck] - check details that need to be verified"
    },
    {
      "line": 23,
      "value": "#[DetailsOfPurchaseOrder] - details we need to verify for purchase order"
    },
    {
      "line": 24,
      "value": "#[PaypalDetailInformation] details that need to be verified for paypal"
    },
    {
      "line": 25,
      "value": "#[Welcome Information] - message for becoming red apple user"
    },
    {
      "line": 26,
      "value": "#[Free_shipping_Msg] - message for free shipping"
    },
    {
      "line": 27,
      "value": "#[Express_Shipping_Msg] - message for express shipping"
    },
    {
      "line": 28,
      "value": "#[SchoolInformation] - information of new school to be edited with current"
    },
    {
      "line": 29,
      "value": "#[Insufficient_BP_Balance] - message for insufficient bonus point"
    },
    {
      "line": 30,
      "value": "#[InvalidCreditCardDetails] - details of invalid credit card"
    },
    {
      "line": 31,
      "value": "#[OrderInformationMoreThan1000$] - ItemId , Quantity that makes order total 1000$"
    },
    {
      "line": 32,
      "value": "#[NewMasterCreditCardDetails] - credit card details for master card"
    }
  ],
  "line": 34,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 35,
  "name": "Parent opens scholastic reading clubs e-commerce site",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginSteps.Parent_opens_scholastic_reading_clubs_ecommerce_site()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "line": 53,
  "name": "Parent changes the child selection and returns back to shopping cart:[InSprint13.4:PCOD:ParentOrder_ChildNameReassign_BackandForth]",
  "description": "",
  "id": "auto-toss-in;parent-changes-the-child-selection-and-returns-back-to-shopping-cart:[\u003cusingdata\u003e];;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@P1"
    },
    {
      "line": 1,
      "name": "@Android"
    },
    {
      "line": 1,
      "name": "@IOS"
    },
    {
      "line": 1,
      "name": "@Automatic_Q_A_Toss-in"
    },
    {
      "line": 1,
      "name": "@Regression"
    },
    {
      "line": 1,
      "name": "@Web"
    }
  ]
});
formatter.step({
  "line": 41,
  "name": "Parent login in with valid [username] and [password]",
  "keyword": "Given "
});
formatter.step({
  "line": 42,
  "name": "parent add item from PDP page and navigates to shopping cart page",
  "keyword": "And "
});
formatter.step({
  "line": 43,
  "name": "the items are assigned to the default child",
  "keyword": "And "
});
formatter.step({
  "line": 44,
  "name": "parent clicks on child dropdown for few items",
  "keyword": "And "
});
formatter.step({
  "line": 45,
  "name": "reassigns them to a different child",
  "keyword": "And "
});
formatter.step({
  "line": 46,
  "name": "Parent navigates to Home page",
  "keyword": "And "
});
formatter.step({
  "line": 47,
  "name": "Parent navigates back to Shopping cart page",
  "keyword": "When "
});
formatter.step({
  "line": 48,
  "name": "all the items that are reassigned are now assigned to the re-assigned child",
  "keyword": "Then "
});
formatter.step({
  "line": 49,
  "name": "the other items are still assigned to default child",
  "keyword": "And "
});
formatter.match({
  "location": "PCOD_LoginSteps.parent_login_in_with_valid_username_and_password()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "PCOD_PDPPageSteps.parent_add_item_from_pdp_page_and_navigates_to_shopping_cart_page()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "PCOD_ReviewCartPageSteps.the_items_are_assigned_to_the_default_child()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "PCOD_ReviewCartPageSteps.parent_clicks_on_child_dropdown_for_few_items()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "PCOD_ReviewCartPageSteps.reassigns_them_to_a_different_child()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "PCOD_ReviewCartPageSteps.parent_navigates_to_home_page()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "PCOD_ReviewCartPageSteps.parent_navigates_back_to_shopping_cart_page()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "PCOD_ReviewCartPageSteps.all_the_items_that_are_reassigned_are_now_assigned_to_the_reassigned_child()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "PCOD_ReviewCartPageSteps.the_other_items_are_still_assigned_to_default_child()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "duration": 8234260082,
  "error_message": "java.lang.RuntimeException: Unable to create local driver\r\n\tat com.scholastic.torque.common.TestBase.initDriver(TestBase.java:158)\r\n\tat com.scholastic.torque.common.TestBase.getDriver(TestBase.java:73)\r\n\tat com.dw.automation.steps.Hooks.afterHook(Hooks.java:104)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)\r\n\tat java.lang.reflect.Method.invoke(Unknown Source)\r\n\tat cucumber.runtime.Utils$1.call(Utils.java:37)\r\n\tat cucumber.runtime.Timeout.timeout(Timeout.java:13)\r\n\tat cucumber.runtime.Utils.invoke(Utils.java:31)\r\n\tat cucumber.runtime.java.JavaHookDefinition.execute(JavaHookDefinition.java:60)\r\n\tat cucumber.runtime.Runtime.runHookIfTagsMatch(Runtime.java:223)\r\n\tat cucumber.runtime.Runtime.runHooks(Runtime.java:211)\r\n\tat cucumber.runtime.Runtime.runAfterHooks(Runtime.java:205)\r\n\tat cucumber.runtime.model.CucumberScenario.run(CucumberScenario.java:46)\r\n\tat cucumber.runtime.model.CucumberScenarioOutline.run(CucumberScenarioOutline.java:46)\r\n\tat cucumber.runtime.model.CucumberFeature.run(CucumberFeature.java:165)\r\n\tat com.scholastic.torque.testng.TestNGCucumberRunner.runCucumber(TestNGCucumberRunner.java:147)\r\n\tat com.scholastic.torque.testng.AbstractTestNGCucumberTests.feature(AbstractTestNGCucumberTests.java:30)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)\r\n\tat java.lang.reflect.Method.invoke(Unknown Source)\r\n\tat org.testng.internal.MethodInvocationHelper.invokeMethod(MethodInvocationHelper.java:85)\r\n\tat org.testng.internal.Invoker.invokeMethod(Invoker.java:639)\r\n\tat org.testng.internal.Invoker.invokeTestMethod(Invoker.java:816)\r\n\tat org.testng.internal.Invoker.invokeTestMethods(Invoker.java:1124)\r\n\tat org.testng.internal.TestMethodWorker.invokeTestMethods(TestMethodWorker.java:125)\r\n\tat org.testng.internal.TestMethodWorker.run(TestMethodWorker.java:108)\r\n\tat org.testng.TestRunner.privateRun(TestRunner.java:774)\r\n\tat org.testng.TestRunner.run(TestRunner.java:624)\r\n\tat org.testng.SuiteRunner.runTest(SuiteRunner.java:359)\r\n\tat org.testng.SuiteRunner.runSequentially(SuiteRunner.java:354)\r\n\tat org.testng.SuiteRunner.privateRun(SuiteRunner.java:312)\r\n\tat org.testng.SuiteRunner.run(SuiteRunner.java:261)\r\n\tat org.testng.SuiteRunnerWorker.runSuite(SuiteRunnerWorker.java:52)\r\n\tat org.testng.SuiteRunnerWorker.run(SuiteRunnerWorker.java:86)\r\n\tat org.testng.TestNG.runSuitesSequentially(TestNG.java:1215)\r\n\tat org.testng.TestNG.runSuitesLocally(TestNG.java:1140)\r\n\tat org.testng.TestNG.run(TestNG.java:1048)\r\n\tat org.apache.maven.surefire.testng.TestNGExecutor.run(TestNGExecutor.java:217)\r\n\tat org.apache.maven.surefire.testng.TestNGXmlTestSuite.execute(TestNGXmlTestSuite.java:84)\r\n\tat org.apache.maven.surefire.testng.TestNGProvider.invoke(TestNGProvider.java:92)\r\n\tat org.apache.maven.surefire.booter.ForkedBooter.invokeProviderInSameClassLoader(ForkedBooter.java:200)\r\n\tat org.apache.maven.surefire.booter.ForkedBooter.runSuitesInProcess(ForkedBooter.java:153)\r\n\tat org.apache.maven.surefire.booter.ForkedBooter.main(ForkedBooter.java:103)\r\nCaused by: org.openqa.selenium.WebDriverException: Unalbe to create webdriver\nBuild info: version: \u00272.53.1\u0027, revision: \u0027a36b8b1cd5757287168e54b817830adce9b0158d\u0027, time: \u00272016-06-30 19:26:09\u0027\nSystem info: host: \u0027RLE0302\u0027, ip: \u002710.32.199.63\u0027, os.name: \u0027Windows 7\u0027, os.arch: \u0027amd64\u0027, os.version: \u00276.1\u0027, java.version: \u00271.8.0_111\u0027\nDriver info: driver.version: unknown\r\n\tat com.scholastic.torque.common.TestBase$WEBDRIVER.getDriver(TestBase.java:378)\r\n\tat com.scholastic.torque.common.TestBase.initDriver(TestBase.java:151)\r\n\t... 45 more\r\nCaused by: org.openqa.selenium.remote.UnreachableBrowserException: Could not start a new session. Possible causes are invalid address of the remote server or browser start-up failure.\nBuild info: version: \u00272.53.1\u0027, revision: \u0027a36b8b1cd5757287168e54b817830adce9b0158d\u0027, time: \u00272016-06-30 19:26:09\u0027\nSystem info: host: \u0027RLE0302\u0027, ip: \u002710.32.199.63\u0027, os.name: \u0027Windows 7\u0027, os.arch: \u0027amd64\u0027, os.version: \u00276.1\u0027, java.version: \u00271.8.0_111\u0027\nDriver info: driver.version: ChromeDriver\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:665)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.startSession(RemoteWebDriver.java:249)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.\u003cinit\u003e(RemoteWebDriver.java:131)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.\u003cinit\u003e(RemoteWebDriver.java:144)\r\n\tat org.openqa.selenium.chrome.ChromeDriver.\u003cinit\u003e(ChromeDriver.java:170)\r\n\tat org.openqa.selenium.chrome.ChromeDriver.\u003cinit\u003e(ChromeDriver.java:159)\r\n\tat org.openqa.selenium.chrome.ChromeDriver.\u003cinit\u003e(ChromeDriver.java:116)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat java.lang.Class.newInstance(Unknown Source)\r\n\tat com.scholastic.torque.common.TestBase$WEBDRIVER.getDriver(TestBase.java:376)\r\n\t... 46 more\r\nCaused by: org.openqa.selenium.WebDriverException: java.net.SocketException: Connection reset\nBuild info: version: \u00272.53.1\u0027, revision: \u0027a36b8b1cd5757287168e54b817830adce9b0158d\u0027, time: \u00272016-06-30 19:26:09\u0027\nSystem info: host: \u0027RLE0302\u0027, ip: \u002710.32.199.63\u0027, os.name: \u0027Windows 7\u0027, os.arch: \u0027amd64\u0027, os.version: \u00276.1\u0027, java.version: \u00271.8.0_111\u0027\nDriver info: driver.version: ChromeDriver\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:91)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:644)\r\n\t... 58 more\r\nCaused by: java.net.SocketException: Connection reset\r\n\tat java.net.SocketInputStream.read(Unknown Source)\r\n\tat java.net.SocketInputStream.read(Unknown Source)\r\n\tat org.apache.http.impl.conn.LoggingInputStream.read(LoggingInputStream.java:87)\r\n\tat org.apache.http.impl.io.SessionInputBufferImpl.streamRead(SessionInputBufferImpl.java:136)\r\n\tat org.apache.http.impl.io.SessionInputBufferImpl.fillBuffer(SessionInputBufferImpl.java:152)\r\n\tat org.apache.http.impl.io.SessionInputBufferImpl.readLine(SessionInputBufferImpl.java:270)\r\n\tat org.apache.http.impl.conn.DefaultHttpResponseParser.parseHead(DefaultHttpResponseParser.java:140)\r\n\tat org.apache.http.impl.conn.DefaultHttpResponseParser.parseHead(DefaultHttpResponseParser.java:57)\r\n\tat org.apache.http.impl.io.AbstractMessageParser.parse(AbstractMessageParser.java:260)\r\n\tat org.apache.http.impl.DefaultBHttpClientConnection.receiveResponseHeader(DefaultBHttpClientConnection.java:161)\r\n\tat org.apache.http.impl.conn.CPoolProxy.receiveResponseHeader(CPoolProxy.java:153)\r\n\tat org.apache.http.protocol.HttpRequestExecutor.doReceiveResponse(HttpRequestExecutor.java:271)\r\n\tat org.apache.http.protocol.HttpRequestExecutor.execute(HttpRequestExecutor.java:123)\r\n\tat org.apache.http.impl.execchain.MainClientExec.execute(MainClientExec.java:254)\r\n\tat org.apache.http.impl.execchain.ProtocolExec.execute(ProtocolExec.java:195)\r\n\tat org.apache.http.impl.execchain.RetryExec.execute(RetryExec.java:86)\r\n\tat org.apache.http.impl.execchain.RedirectExec.execute(RedirectExec.java:108)\r\n\tat org.apache.http.impl.client.InternalHttpClient.doExecute(InternalHttpClient.java:184)\r\n\tat org.apache.http.impl.client.CloseableHttpClient.execute(CloseableHttpClient.java:72)\r\n\tat org.apache.http.impl.client.CloseableHttpClient.execute(CloseableHttpClient.java:57)\r\n\tat org.openqa.selenium.remote.internal.ApacheHttpClient.fallBackExecute(ApacheHttpClient.java:144)\r\n\tat org.openqa.selenium.remote.internal.ApacheHttpClient.execute(ApacheHttpClient.java:90)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:142)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:82)\r\n\t... 59 more\r\n",
  "status": "failed"
});
});