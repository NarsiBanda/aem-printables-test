$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("features/testfeature.feature");
formatter.feature({
  "line": 1,
  "name": "Test Feature",
  "description": "This feature is used to check login",
  "id": "test-feature",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "comments": [
    {
      "line": 7,
      "value": "########################## Teacher Checkout Test Case ################################"
    }
  ],
  "line": 9,
  "name": "Parent enters a non-existing coupon code:[\u003cUsingData\u003e]",
  "description": "",
  "id": "test-feature;parent-enters-a-non-existing-coupon-code:[\u003cusingdata\u003e]",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 11,
  "name": "teacher login into the application by providing [UserInformation]",
  "keyword": "Given "
});
formatter.examples({
  "line": 15,
  "name": "",
  "description": "",
  "id": "test-feature;parent-enters-a-non-existing-coupon-code:[\u003cusingdata\u003e];",
  "rows": [
    {
      "cells": [
        "UsingData"
      ],
      "line": 16,
      "id": "test-feature;parent-enters-a-non-existing-coupon-code:[\u003cusingdata\u003e];;1"
    },
    {
      "cells": [
        "ParentLogin"
      ],
      "line": 17,
      "id": "test-feature;parent-enters-a-non-existing-coupon-code:[\u003cusingdata\u003e];;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 42606001385,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "teacher open rco-scholastic web site",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginSteps.user_open_rco_scholastic_web_site()"
});
formatter.result({
  "duration": 132602910,
  "status": "passed"
});
formatter.scenario({
  "line": 17,
  "name": "Parent enters a non-existing coupon code:[ParentLogin]",
  "description": "",
  "id": "test-feature;parent-enters-a-non-existing-coupon-code:[\u003cusingdata\u003e];;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 11,
  "name": "teacher login into the application by providing [UserInformation]",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginSteps.teacher_login_into_the_application_by_providing_userinformation()"
});
formatter.result({
  "duration": 13016840642,
  "status": "passed"
});
formatter.after({
  "duration": 642492553,
  "status": "passed"
});
});