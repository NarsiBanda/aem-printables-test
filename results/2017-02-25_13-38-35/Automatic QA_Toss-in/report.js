$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("features/testfeature.feature");
formatter.feature({
  "line": 2,
  "name": "Auto Toss-in",
  "description": "This feature is used to check for the Automatic Toss-in features",
  "id": "auto-toss-in",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@Automatic_Q_A_Toss-in"
    },
    {
      "line": 1,
      "name": "@P1"
    },
    {
      "line": 1,
      "name": "@Regression"
    },
    {
      "line": 1,
      "name": "@Android"
    },
    {
      "line": 1,
      "name": "@IOS"
    },
    {
      "line": 1,
      "name": "@Web"
    }
  ]
});
formatter.scenarioOutline({
  "comments": [
    {
      "line": 37,
      "value": "########################## Teacher Checkout Test Case ################################"
    }
  ],
  "line": 42,
  "name": "Parent enters a non-existing coupon code:[\u003cUsingData\u003e]",
  "description": "",
  "id": "auto-toss-in;parent-enters-a-non-existing-coupon-code:[\u003cusingdata\u003e]",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 39,
      "name": "@DWCAPCOD-105"
    },
    {
      "line": 40,
      "name": "@ReviewCart"
    },
    {
      "line": 41,
      "name": "@Priority1"
    }
  ]
});
formatter.step({
  "line": 44,
  "name": "Parent login in with valid [username] and [password]",
  "keyword": "Given "
});
formatter.step({
  "line": 45,
  "name": "parent add item from PDP page and navigates to shopping cart page",
  "keyword": "And "
});
formatter.step({
  "line": 46,
  "name": "parent enters non-existing coupon code",
  "keyword": "When "
});
formatter.step({
  "line": 47,
  "name": "parent clicks “Apply” button",
  "keyword": "And "
});
formatter.step({
  "line": 48,
  "name": "error message “The system does not recognize the coupon code you have entered. Please check the coupon and try again. If the coupon code is correct, please contact customer service at 1-800-SCHOLASTIC (1-800-724-6527).” displays",
  "keyword": "Then "
});
formatter.examples({
  "line": 51,
  "name": "",
  "description": "",
  "id": "auto-toss-in;parent-enters-a-non-existing-coupon-code:[\u003cusingdata\u003e];",
  "rows": [
    {
      "cells": [
        "UsingData"
      ],
      "line": 52,
      "id": "auto-toss-in;parent-enters-a-non-existing-coupon-code:[\u003cusingdata\u003e];;1"
    },
    {
      "cells": [
        "InSprint13.3:PCOD:Parent_Non-ExistingCouponCode_ShoppingCart"
      ],
      "line": 53,
      "id": "auto-toss-in;parent-enters-a-non-existing-coupon-code:[\u003cusingdata\u003e];;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 24328889347,
  "status": "passed"
});
formatter.background({
  "comments": [
    {
      "line": 5,
      "value": "#[UserInformation]- UserName,Password"
    },
    {
      "line": 6,
      "value": "#[ItemInformation]-StudentName,ItemId"
    },
    {
      "line": 7,
      "value": "#[OrderSummary]-Bonus Points Used ,Bonus Points Earned ,Student Online Orders (Paid) ,Student Flyer Orders,Your Teacher Order"
    },
    {
      "line": 8,
      "value": "#[OrderInformation]-ItemId , Quantity and other attributes related to item."
    },
    {
      "line": 9,
      "value": "#[Free_Shipping_msg]- \u0027shipping fee is $3.50\u0027"
    },
    {
      "line": 10,
      "value": "#[Shipping_Charge_Msg]- \u0027Shipping charge as $3.50\u0027"
    },
    {
      "line": 11,
      "value": "#[Due_date_message] - \u0027Due Day message\u0027"
    },
    {
      "line": 12,
      "value": "#[Error_Msg] - \u0027Error in Cart\u0027"
    },
    {
      "line": 13,
      "value": "#[Due_date_message] - \u0027Your Next Due Date is Date\u0027"
    },
    {
      "line": 14,
      "value": "#[OS/SD/PM on submitorder] - order summary, shipping detail , payment method on submit-order-page"
    },
    {
      "line": 15,
      "value": "#[Paypal_Account_Detail] - username,id,password for new paypal account creation"
    },
    {
      "line": 16,
      "value": "#[CreditCardInformation] - nameOfCard, cardNumber, exp-date, cvv, firstName, lastName, address"
    },
    {
      "line": 17,
      "value": "#[OrderInformationMoreThan1000$] - ItemId , Quantity ,Payment Information of item more than 1000$"
    },
    {
      "line": 18,
      "value": "# [Email-idErrorMessage] - error message for no email id"
    },
    {
      "line": 19,
      "value": "#[PaymentInformation] - details for making payment"
    },
    {
      "line": 20,
      "value": "#[PaymentMethodUpdatedMessage] - message for updatde payment method in wallet of paypal"
    },
    {
      "line": 21,
      "value": "#[ShippingInformation] - shipping address details"
    },
    {
      "line": 22,
      "value": "#[DetailsOfCheck] - check details that need to be verified"
    },
    {
      "line": 23,
      "value": "#[DetailsOfPurchaseOrder] - details we need to verify for purchase order"
    },
    {
      "line": 24,
      "value": "#[PaypalDetailInformation] details that need to be verified for paypal"
    },
    {
      "line": 25,
      "value": "#[Welcome Information] - message for becoming red apple user"
    },
    {
      "line": 26,
      "value": "#[Free_shipping_Msg] - message for free shipping"
    },
    {
      "line": 27,
      "value": "#[Express_Shipping_Msg] - message for express shipping"
    },
    {
      "line": 28,
      "value": "#[SchoolInformation] - information of new school to be edited with current"
    },
    {
      "line": 29,
      "value": "#[Insufficient_BP_Balance] - message for insufficient bonus point"
    },
    {
      "line": 30,
      "value": "#[InvalidCreditCardDetails] - details of invalid credit card"
    },
    {
      "line": 31,
      "value": "#[OrderInformationMoreThan1000$] - ItemId , Quantity that makes order total 1000$"
    },
    {
      "line": 32,
      "value": "#[NewMasterCreditCardDetails] - credit card details for master card"
    }
  ],
  "line": 34,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 35,
  "name": "Parent opens scholastic reading clubs e-commerce site",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginSteps.Parent_opens_scholastic_reading_clubs_ecommerce_site()"
});
formatter.result({
  "duration": 6524016373,
  "status": "passed"
});
formatter.scenario({
  "line": 53,
  "name": "Parent enters a non-existing coupon code:[InSprint13.3:PCOD:Parent_Non-ExistingCouponCode_ShoppingCart]",
  "description": "",
  "id": "auto-toss-in;parent-enters-a-non-existing-coupon-code:[\u003cusingdata\u003e];;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@P1"
    },
    {
      "line": 1,
      "name": "@Android"
    },
    {
      "line": 1,
      "name": "@IOS"
    },
    {
      "line": 1,
      "name": "@Automatic_Q_A_Toss-in"
    },
    {
      "line": 39,
      "name": "@DWCAPCOD-105"
    },
    {
      "line": 1,
      "name": "@Regression"
    },
    {
      "line": 41,
      "name": "@Priority1"
    },
    {
      "line": 1,
      "name": "@Web"
    },
    {
      "line": 40,
      "name": "@ReviewCart"
    }
  ]
});
formatter.step({
  "line": 44,
  "name": "Parent login in with valid [username] and [password]",
  "keyword": "Given "
});
formatter.step({
  "line": 45,
  "name": "parent add item from PDP page and navigates to shopping cart page",
  "keyword": "And "
});
formatter.step({
  "line": 46,
  "name": "parent enters non-existing coupon code",
  "keyword": "When "
});
formatter.step({
  "line": 47,
  "name": "parent clicks “Apply” button",
  "keyword": "And "
});
formatter.step({
  "line": 48,
  "name": "error message “The system does not recognize the coupon code you have entered. Please check the coupon and try again. If the coupon code is correct, please contact customer service at 1-800-SCHOLASTIC (1-800-724-6527).” displays",
  "keyword": "Then "
});
formatter.match({
  "location": "PCOD_LoginSteps.parent_login_in_with_valid_username_and_password()"
});
formatter.result({
  "duration": 55953885615,
  "status": "passed"
});
formatter.match({
  "location": "PCOD_PDPPageSteps.parent_add_item_from_pdp_page_and_navigates_to_shopping_cart_page()"
});
formatter.result({
  "duration": 17149963424,
  "status": "passed"
});
formatter.match({
  "location": "PCOD_ReviewCartPageSteps.parent_enters_nonexisting_coupon_code()"
});
formatter.result({
  "duration": 3417252936,
  "status": "passed"
});
formatter.match({
  "location": "PCOD_ReviewCartPageSteps.parent_clicks_apply_button()"
});
formatter.result({
  "duration": 3292888814,
  "status": "passed"
});
formatter.match({
  "location": "PCOD_ReviewCartPageSteps.error_message_the_system_does_not_recognize_the_coupon_code_you_have_entered_please_check_the_coupon_and_try_again_if_the_coupon_code_is_correct_please_contact_customer_service_at_1800scholastic_18007246527_displays()"
});
formatter.result({
  "duration": 5962379682,
  "error_message": "java.lang.AssertionError: FreePick coupon value is incorrect expected [The system does not recognize the coupon code \"ABCED\" you have entered. Please check the coupon and try again. If the coupon code is correct, please contact Customer Service at 1-800-SCHOLASTIC (1-800-724-6527).] but found [The system does not recognize the coupon code \"ABCED\" you have entered. Please check the coupon and try again. If the coupon code is correct, please contact Customer Service at 1-800-SCHOLASTIC (1-800-268-3860).]\r\n\tat org.testng.Assert.fail(Assert.java:94)\r\n\tat org.testng.Assert.failNotEquals(Assert.java:496)\r\n\tat org.testng.Assert.assertEquals(Assert.java:125)\r\n\tat org.testng.Assert.assertEquals(Assert.java:178)\r\n\tat com.dw.automation.pages.impl.PCOD_LoginPage.pcod_gettext_equals(PCOD_LoginPage.java:2100)\r\n\tat com.dw.automation.steps.home.PCOD_ReviewCartPageSteps.error_message_the_system_does_not_recognize_the_coupon_code_you_have_entered_please_check_the_coupon_and_try_again_if_the_coupon_code_is_correct_please_contact_customer_service_at_1800scholastic_18007246527_displays(PCOD_ReviewCartPageSteps.java:393)\r\n\tat ✽.Then error message “The system does not recognize the coupon code you have entered. Please check the coupon and try again. If the coupon code is correct, please contact customer service at 1-800-SCHOLASTIC (1-800-724-6527).” displays(features/testfeature.feature:48)\r\n",
  "status": "failed"
});
formatter.write("Current Page URL is https://dev36-rco-scholastic.demandware.net/s/rco-ca/checkout");
formatter.embedding("image/png", "embedded0.png");
formatter.after({
  "duration": 976752464,
  "status": "passed"
});
});