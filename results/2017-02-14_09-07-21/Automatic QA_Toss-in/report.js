$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("features/testfeature.feature");
formatter.feature({
  "line": 2,
  "name": "Auto Toss-in",
  "description": "This feature is used to check for the Automatic Toss-in features",
  "id": "auto-toss-in",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@Automatic_Q_A_Toss-in"
    },
    {
      "line": 1,
      "name": "@P1"
    },
    {
      "line": 1,
      "name": "@Regression"
    },
    {
      "line": 1,
      "name": "@Android"
    },
    {
      "line": 1,
      "name": "@IOS"
    },
    {
      "line": 1,
      "name": "@Web"
    }
  ]
});
formatter.scenarioOutline({
  "comments": [
    {
      "line": 37,
      "value": "########################## Teacher Checkout Test Case ################################"
    }
  ],
  "line": 39,
  "name": "Parent reassigns few items to child and rest of the items stay assigned to default child:[\u003cUsingData\u003e]",
  "description": "",
  "id": "auto-toss-in;parent-reassigns-few-items-to-child-and-rest-of-the-items-stay-assigned-to-default-child:[\u003cusingdata\u003e]",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 41,
  "name": "Parent login in with valid [username] and [password]",
  "keyword": "Given "
});
formatter.step({
  "line": 42,
  "name": "parent add 2 items to cart and navigate to shopping cart",
  "keyword": "And "
});
formatter.step({
  "line": 43,
  "name": "the items are assigned to the default child",
  "keyword": "And "
});
formatter.step({
  "line": 44,
  "name": "parent clicks on child dropdown for few items",
  "keyword": "When "
});
formatter.step({
  "line": 45,
  "name": "reassigns them to a different child",
  "keyword": "And "
});
formatter.step({
  "line": 46,
  "name": "all the items that are reassigned are now assigned to the re-assigned child",
  "keyword": "Then "
});
formatter.step({
  "line": 47,
  "name": "the other items are still assigned to default child",
  "keyword": "And "
});
formatter.examples({
  "line": 49,
  "name": "",
  "description": "",
  "id": "auto-toss-in;parent-reassigns-few-items-to-child-and-rest-of-the-items-stay-assigned-to-default-child:[\u003cusingdata\u003e];",
  "rows": [
    {
      "cells": [
        "UsingData"
      ],
      "line": 50,
      "id": "auto-toss-in;parent-reassigns-few-items-to-child-and-rest-of-the-items-stay-assigned-to-default-child:[\u003cusingdata\u003e];;1"
    },
    {
      "cells": [
        "InSprint13.4:PCOD:ParentOrder_ChildOrder_Dropdown_Different"
      ],
      "line": 51,
      "id": "auto-toss-in;parent-reassigns-few-items-to-child-and-rest-of-the-items-stay-assigned-to-default-child:[\u003cusingdata\u003e];;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 43166730813,
  "status": "passed"
});
formatter.background({
  "comments": [
    {
      "line": 5,
      "value": "#[UserInformation]- UserName,Password"
    },
    {
      "line": 6,
      "value": "#[ItemInformation]-StudentName,ItemId"
    },
    {
      "line": 7,
      "value": "#[OrderSummary]-Bonus Points Used ,Bonus Points Earned ,Student Online Orders (Paid) ,Student Flyer Orders,Your Teacher Order"
    },
    {
      "line": 8,
      "value": "#[OrderInformation]-ItemId , Quantity and other attributes related to item."
    },
    {
      "line": 9,
      "value": "#[Free_Shipping_msg]- \u0027shipping fee is $3.50\u0027"
    },
    {
      "line": 10,
      "value": "#[Shipping_Charge_Msg]- \u0027Shipping charge as $3.50\u0027"
    },
    {
      "line": 11,
      "value": "#[Due_date_message] - \u0027Due Day message\u0027"
    },
    {
      "line": 12,
      "value": "#[Error_Msg] - \u0027Error in Cart\u0027"
    },
    {
      "line": 13,
      "value": "#[Due_date_message] - \u0027Your Next Due Date is Date\u0027"
    },
    {
      "line": 14,
      "value": "#[OS/SD/PM on submitorder] - order summary, shipping detail , payment method on submit-order-page"
    },
    {
      "line": 15,
      "value": "#[Paypal_Account_Detail] - username,id,password for new paypal account creation"
    },
    {
      "line": 16,
      "value": "#[CreditCardInformation] - nameOfCard, cardNumber, exp-date, cvv, firstName, lastName, address"
    },
    {
      "line": 17,
      "value": "#[OrderInformationMoreThan1000$] - ItemId , Quantity ,Payment Information of item more than 1000$"
    },
    {
      "line": 18,
      "value": "# [Email-idErrorMessage] - error message for no email id"
    },
    {
      "line": 19,
      "value": "#[PaymentInformation] - details for making payment"
    },
    {
      "line": 20,
      "value": "#[PaymentMethodUpdatedMessage] - message for updatde payment method in wallet of paypal"
    },
    {
      "line": 21,
      "value": "#[ShippingInformation] - shipping address details"
    },
    {
      "line": 22,
      "value": "#[DetailsOfCheck] - check details that need to be verified"
    },
    {
      "line": 23,
      "value": "#[DetailsOfPurchaseOrder] - details we need to verify for purchase order"
    },
    {
      "line": 24,
      "value": "#[PaypalDetailInformation] details that need to be verified for paypal"
    },
    {
      "line": 25,
      "value": "#[Welcome Information] - message for becoming red apple user"
    },
    {
      "line": 26,
      "value": "#[Free_shipping_Msg] - message for free shipping"
    },
    {
      "line": 27,
      "value": "#[Express_Shipping_Msg] - message for express shipping"
    },
    {
      "line": 28,
      "value": "#[SchoolInformation] - information of new school to be edited with current"
    },
    {
      "line": 29,
      "value": "#[Insufficient_BP_Balance] - message for insufficient bonus point"
    },
    {
      "line": 30,
      "value": "#[InvalidCreditCardDetails] - details of invalid credit card"
    },
    {
      "line": 31,
      "value": "#[OrderInformationMoreThan1000$] - ItemId , Quantity that makes order total 1000$"
    },
    {
      "line": 32,
      "value": "#[NewMasterCreditCardDetails] - credit card details for master card"
    }
  ],
  "line": 34,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 35,
  "name": "Parent opens scholastic reading clubs e-commerce site",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginSteps.Parent_opens_scholastic_reading_clubs_ecommerce_site()"
});
formatter.result({
  "duration": 6581108133,
  "status": "passed"
});
formatter.scenario({
  "line": 51,
  "name": "Parent reassigns few items to child and rest of the items stay assigned to default child:[InSprint13.4:PCOD:ParentOrder_ChildOrder_Dropdown_Different]",
  "description": "",
  "id": "auto-toss-in;parent-reassigns-few-items-to-child-and-rest-of-the-items-stay-assigned-to-default-child:[\u003cusingdata\u003e];;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@P1"
    },
    {
      "line": 1,
      "name": "@Android"
    },
    {
      "line": 1,
      "name": "@IOS"
    },
    {
      "line": 1,
      "name": "@Automatic_Q_A_Toss-in"
    },
    {
      "line": 1,
      "name": "@Regression"
    },
    {
      "line": 1,
      "name": "@Web"
    }
  ]
});
formatter.step({
  "line": 41,
  "name": "Parent login in with valid [username] and [password]",
  "keyword": "Given "
});
formatter.step({
  "line": 42,
  "name": "parent add 2 items to cart and navigate to shopping cart",
  "keyword": "And "
});
formatter.step({
  "line": 43,
  "name": "the items are assigned to the default child",
  "keyword": "And "
});
formatter.step({
  "line": 44,
  "name": "parent clicks on child dropdown for few items",
  "keyword": "When "
});
formatter.step({
  "line": 45,
  "name": "reassigns them to a different child",
  "keyword": "And "
});
formatter.step({
  "line": 46,
  "name": "all the items that are reassigned are now assigned to the re-assigned child",
  "keyword": "Then "
});
formatter.step({
  "line": 47,
  "name": "the other items are still assigned to default child",
  "keyword": "And "
});
formatter.match({
  "location": "PCOD_LoginSteps.parent_login_in_with_valid_username_and_password()"
});
formatter.result({
  "duration": 127440912240,
  "status": "passed"
});
formatter.match({
  "location": "PCOD_PDPPageSteps.parent_add_2_items_to_cart_and_navigate_to_shopping_cart()"
});
formatter.result({
  "duration": 40425503904,
  "status": "passed"
});
formatter.match({
  "location": "PCOD_ReviewCartPageSteps.the_items_are_assigned_to_the_default_child()"
});
formatter.result({
  "duration": 5466078381,
  "status": "passed"
});
formatter.match({
  "location": "PCOD_ReviewCartPageSteps.parent_clicks_on_child_dropdown_for_few_items()"
});
formatter.result({
  "duration": 3248417669,
  "status": "passed"
});
formatter.match({
  "location": "PCOD_ReviewCartPageSteps.reassigns_them_to_a_different_child()"
});
formatter.result({
  "duration": 2451945053,
  "status": "passed"
});
formatter.match({
  "location": "PCOD_ReviewCartPageSteps.all_the_items_that_are_reassigned_are_now_assigned_to_the_reassigned_child()"
});
formatter.result({
  "duration": 2641608293,
  "status": "passed"
});
formatter.match({
  "location": "PCOD_ReviewCartPageSteps.the_other_items_are_still_assigned_to_default_child()"
});
formatter.result({
  "duration": 2691831874,
  "status": "passed"
});
formatter.after({
  "duration": 645025577,
  "status": "passed"
});
});