$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("features/testfeature.feature");
formatter.feature({
  "line": 2,
  "name": "Auto Toss-in",
  "description": "This feature is used to check for the Automatic Toss-in features",
  "id": "auto-toss-in",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@Automatic_Q_A_Toss-in"
    },
    {
      "line": 1,
      "name": "@P1"
    },
    {
      "line": 1,
      "name": "@Regression"
    },
    {
      "line": 1,
      "name": "@Android"
    },
    {
      "line": 1,
      "name": "@IOS"
    },
    {
      "line": 1,
      "name": "@Web"
    }
  ]
});
formatter.scenarioOutline({
  "comments": [
    {
      "line": 37,
      "value": "########################## Teacher Checkout Test Case ################################"
    }
  ],
  "line": 39,
  "name": "Shipping address for parent with 2 child connected to same teacher on Submit Your Order page:[\u003cUsingData\u003e]",
  "description": "",
  "id": "auto-toss-in;shipping-address-for-parent-with-2-child-connected-to-same-teacher-on-submit-your-order-page:[\u003cusingdata\u003e]",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 41,
  "name": "parent has 2 childs connected to same teacher",
  "keyword": "Given "
});
formatter.step({
  "line": 42,
  "name": "Parent login in with valid [username] and [password]",
  "keyword": "And "
});
formatter.step({
  "line": 43,
  "name": "parent add 2 items to cart and navigate to shopping cart",
  "keyword": "And "
});
formatter.step({
  "line": 44,
  "name": "reassigns few items to child2",
  "keyword": "And "
});
formatter.step({
  "line": 45,
  "name": "parent navigates to Submit Your Order page page",
  "keyword": "When "
});
formatter.step({
  "line": 46,
  "name": "Shipping section displays with “\u003cChild1name\u003e\u0027s items will be shipped to:”",
  "keyword": "Then "
});
formatter.step({
  "line": 47,
  "name": "teacher\u0027s first and last name with school address \u003cSchool Name, Street, city,Country, postal code\u003e displays",
  "keyword": "And "
});
formatter.step({
  "line": 48,
  "name": "“\u003cChild2name\u003e\u0027s items will be shipped to:”",
  "keyword": "And "
});
formatter.step({
  "line": 49,
  "name": "teacher\u0027s first and last name with school address \u003cSchool Name, Street, city,Country, postal code\u003e displays",
  "keyword": "And "
});
formatter.examples({
  "line": 51,
  "name": "",
  "description": "",
  "id": "auto-toss-in;shipping-address-for-parent-with-2-child-connected-to-same-teacher-on-submit-your-order-page:[\u003cusingdata\u003e];",
  "rows": [
    {
      "cells": [
        "UsingData"
      ],
      "line": 52,
      "id": "auto-toss-in;shipping-address-for-parent-with-2-child-connected-to-same-teacher-on-submit-your-order-page:[\u003cusingdata\u003e];;1"
    },
    {
      "cells": [
        "InSprint13.4:PCOD:ParentOrder_SubmitYourOrder_ShippingSection_2Child"
      ],
      "line": 53,
      "id": "auto-toss-in;shipping-address-for-parent-with-2-child-connected-to-same-teacher-on-submit-your-order-page:[\u003cusingdata\u003e];;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 27949121649,
  "status": "passed"
});
formatter.background({
  "comments": [
    {
      "line": 5,
      "value": "#[UserInformation]- UserName,Password"
    },
    {
      "line": 6,
      "value": "#[ItemInformation]-StudentName,ItemId"
    },
    {
      "line": 7,
      "value": "#[OrderSummary]-Bonus Points Used ,Bonus Points Earned ,Student Online Orders (Paid) ,Student Flyer Orders,Your Teacher Order"
    },
    {
      "line": 8,
      "value": "#[OrderInformation]-ItemId , Quantity and other attributes related to item."
    },
    {
      "line": 9,
      "value": "#[Free_Shipping_msg]- \u0027shipping fee is $3.50\u0027"
    },
    {
      "line": 10,
      "value": "#[Shipping_Charge_Msg]- \u0027Shipping charge as $3.50\u0027"
    },
    {
      "line": 11,
      "value": "#[Due_date_message] - \u0027Due Day message\u0027"
    },
    {
      "line": 12,
      "value": "#[Error_Msg] - \u0027Error in Cart\u0027"
    },
    {
      "line": 13,
      "value": "#[Due_date_message] - \u0027Your Next Due Date is Date\u0027"
    },
    {
      "line": 14,
      "value": "#[OS/SD/PM on submitorder] - order summary, shipping detail , payment method on submit-order-page"
    },
    {
      "line": 15,
      "value": "#[Paypal_Account_Detail] - username,id,password for new paypal account creation"
    },
    {
      "line": 16,
      "value": "#[CreditCardInformation] - nameOfCard, cardNumber, exp-date, cvv, firstName, lastName, address"
    },
    {
      "line": 17,
      "value": "#[OrderInformationMoreThan1000$] - ItemId , Quantity ,Payment Information of item more than 1000$"
    },
    {
      "line": 18,
      "value": "# [Email-idErrorMessage] - error message for no email id"
    },
    {
      "line": 19,
      "value": "#[PaymentInformation] - details for making payment"
    },
    {
      "line": 20,
      "value": "#[PaymentMethodUpdatedMessage] - message for updatde payment method in wallet of paypal"
    },
    {
      "line": 21,
      "value": "#[ShippingInformation] - shipping address details"
    },
    {
      "line": 22,
      "value": "#[DetailsOfCheck] - check details that need to be verified"
    },
    {
      "line": 23,
      "value": "#[DetailsOfPurchaseOrder] - details we need to verify for purchase order"
    },
    {
      "line": 24,
      "value": "#[PaypalDetailInformation] details that need to be verified for paypal"
    },
    {
      "line": 25,
      "value": "#[Welcome Information] - message for becoming red apple user"
    },
    {
      "line": 26,
      "value": "#[Free_shipping_Msg] - message for free shipping"
    },
    {
      "line": 27,
      "value": "#[Express_Shipping_Msg] - message for express shipping"
    },
    {
      "line": 28,
      "value": "#[SchoolInformation] - information of new school to be edited with current"
    },
    {
      "line": 29,
      "value": "#[Insufficient_BP_Balance] - message for insufficient bonus point"
    },
    {
      "line": 30,
      "value": "#[InvalidCreditCardDetails] - details of invalid credit card"
    },
    {
      "line": 31,
      "value": "#[OrderInformationMoreThan1000$] - ItemId , Quantity that makes order total 1000$"
    },
    {
      "line": 32,
      "value": "#[NewMasterCreditCardDetails] - credit card details for master card"
    }
  ],
  "line": 34,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 35,
  "name": "Parent opens scholastic reading clubs e-commerce site",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginSteps.Parent_opens_scholastic_reading_clubs_ecommerce_site()"
});
formatter.result({
  "duration": 6446300802,
  "status": "passed"
});
formatter.scenario({
  "line": 53,
  "name": "Shipping address for parent with 2 child connected to same teacher on Submit Your Order page:[InSprint13.4:PCOD:ParentOrder_SubmitYourOrder_ShippingSection_2Child]",
  "description": "",
  "id": "auto-toss-in;shipping-address-for-parent-with-2-child-connected-to-same-teacher-on-submit-your-order-page:[\u003cusingdata\u003e];;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@P1"
    },
    {
      "line": 1,
      "name": "@Android"
    },
    {
      "line": 1,
      "name": "@IOS"
    },
    {
      "line": 1,
      "name": "@Automatic_Q_A_Toss-in"
    },
    {
      "line": 1,
      "name": "@Regression"
    },
    {
      "line": 1,
      "name": "@Web"
    }
  ]
});
formatter.step({
  "line": 41,
  "name": "parent has 2 childs connected to same teacher",
  "keyword": "Given "
});
formatter.step({
  "line": 42,
  "name": "Parent login in with valid [username] and [password]",
  "keyword": "And "
});
formatter.step({
  "line": 43,
  "name": "parent add 2 items to cart and navigate to shopping cart",
  "keyword": "And "
});
formatter.step({
  "line": 44,
  "name": "reassigns few items to child2",
  "keyword": "And "
});
formatter.step({
  "line": 45,
  "name": "parent navigates to Submit Your Order page page",
  "keyword": "When "
});
formatter.step({
  "line": 46,
  "name": "Shipping section displays with “\u003cChild1name\u003e\u0027s items will be shipped to:”",
  "keyword": "Then "
});
formatter.step({
  "line": 47,
  "name": "teacher\u0027s first and last name with school address \u003cSchool Name, Street, city,Country, postal code\u003e displays",
  "keyword": "And "
});
formatter.step({
  "line": 48,
  "name": "“\u003cChild2name\u003e\u0027s items will be shipped to:”",
  "keyword": "And "
});
formatter.step({
  "line": 49,
  "name": "teacher\u0027s first and last name with school address \u003cSchool Name, Street, city,Country, postal code\u003e displays",
  "keyword": "And "
});
formatter.match({
  "location": "PCOD_LoginSteps.parent_has_2_childs_connected_to_same_teacher()"
});
formatter.result({
  "duration": 23625324,
  "status": "passed"
});
formatter.match({
  "location": "PCOD_LoginSteps.parent_login_in_with_valid_username_and_password()"
});
formatter.result({
  "duration": 156985677663,
  "status": "passed"
});
formatter.match({
  "location": "PCOD_PDPPageSteps.parent_add_2_items_to_cart_and_navigate_to_shopping_cart()"
});
formatter.result({
  "duration": 47871686327,
  "status": "passed"
});
formatter.match({
  "location": "PCOD_ReviewCartPageSteps.reassigns_few_items_to_child2()"
});
formatter.result({
  "duration": 2212393254,
  "status": "passed"
});
formatter.match({
  "location": "PCOD_ReviewCartPageSteps.parent_navigates_to_submit_your_order_page_page()"
});
formatter.result({
  "duration": 33266276029,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "\u003cChild1name\u003e",
      "offset": 32
    }
  ],
  "location": "PCOD_ShippingAndPaymentPageSteps.shipping_section_displays_with_s_items_will_be_shipped_to(String)"
});
formatter.result({
  "duration": 12936173230,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "\u003cSchool Name, Street, city,Country, postal code\u003e",
      "offset": 50
    }
  ],
  "location": "PCOD_ShippingAndPaymentPageSteps.teachers_first_and_last_name_with_school_address_displays(String)"
});
formatter.result({
  "duration": 109703499,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "\u003cChild2name\u003e",
      "offset": 1
    }
  ],
  "location": "PCOD_ShippingAndPaymentPageSteps.s_items_will_be_shipped_to(String)"
});
formatter.result({
  "duration": 2795723359,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "\u003cSchool Name, Street, city,Country, postal code\u003e",
      "offset": 50
    }
  ],
  "location": "PCOD_ShippingAndPaymentPageSteps.teachers_first_and_last_name_with_school_address_displays(String)"
});
formatter.result({
  "duration": 63726001,
  "status": "passed"
});
formatter.after({
  "duration": 573690754,
  "status": "passed"
});
});