$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("features/testfeature.feature");
formatter.feature({
  "line": 2,
  "name": "Auto Toss-in",
  "description": "This feature is used to check for the Automatic Toss-in features",
  "id": "auto-toss-in",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@Automatic_Q_A_Toss-in"
    },
    {
      "line": 1,
      "name": "@P1"
    },
    {
      "line": 1,
      "name": "@Regression"
    },
    {
      "line": 1,
      "name": "@Android"
    },
    {
      "line": 1,
      "name": "@IOS"
    },
    {
      "line": 1,
      "name": "@Web"
    }
  ]
});
formatter.scenarioOutline({
  "comments": [
    {
      "line": 37,
      "value": "########################## Teacher Checkout Test Case ################################"
    }
  ],
  "line": 42,
  "name": "Parent enters a non-existing coupon code:[\u003cUsingData\u003e]",
  "description": "",
  "id": "auto-toss-in;parent-enters-a-non-existing-coupon-code:[\u003cusingdata\u003e]",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 39,
      "name": "@DWCAPCOD-105"
    },
    {
      "line": 40,
      "name": "@ReviewCart"
    },
    {
      "line": 41,
      "name": "@Priority1"
    }
  ]
});
formatter.step({
  "line": 44,
  "name": "Parent login in with valid [username] and [password]",
  "keyword": "Given "
});
formatter.step({
  "line": 45,
  "name": "parent add item from PDP page and navigates to shopping cart page",
  "keyword": "And "
});
formatter.step({
  "line": 46,
  "name": "parent enters non-existing coupon code",
  "keyword": "When "
});
formatter.step({
  "line": 47,
  "name": "parent clicks “Apply” button",
  "keyword": "And "
});
formatter.step({
  "line": 48,
  "name": "error message “The system does not recognize the coupon code you have entered. Please check the coupon and try again. If the coupon code is correct, please contact customer service at 1-800-SCHOLASTIC (1-800-724-6527).” displays",
  "keyword": "Then "
});
formatter.examples({
  "line": 51,
  "name": "",
  "description": "",
  "id": "auto-toss-in;parent-enters-a-non-existing-coupon-code:[\u003cusingdata\u003e];",
  "rows": [
    {
      "cells": [
        "UsingData"
      ],
      "line": 52,
      "id": "auto-toss-in;parent-enters-a-non-existing-coupon-code:[\u003cusingdata\u003e];;1"
    },
    {
      "cells": [
        "InSprint13.3:PCOD:Parent_Non-ExistingCouponCode_ShoppingCart"
      ],
      "line": 53,
      "id": "auto-toss-in;parent-enters-a-non-existing-coupon-code:[\u003cusingdata\u003e];;2"
    }
  ],
  "keyword": "Examples"
});
